package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Inmueble;
import com.valkyrie.proyintmaven.Pago;
import com.valkyrie.proyintmaven.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Reserva.class)
public class Reserva_ { 

    public static volatile SingularAttribute<Reserva, String> rsvaEstado;
    public static volatile SingularAttribute<Reserva, Integer> rsvaMenoredad;
    public static volatile CollectionAttribute<Reserva, Inmueble> inmuebleCollection;
    public static volatile SingularAttribute<Reserva, Integer> rsvaNoche;
    public static volatile SingularAttribute<Reserva, Date> rsvaCheckout;
    public static volatile SingularAttribute<Reserva, Date> rsvaCheckin;
    public static volatile CollectionAttribute<Reserva, Usuario> usuarioCollection;
    public static volatile SingularAttribute<Reserva, Date> rsvaFecharealizada;
    public static volatile SingularAttribute<Reserva, Integer> rsvaAdulto;
    public static volatile SingularAttribute<Reserva, Integer> rsvaFechallegada;
    public static volatile SingularAttribute<Reserva, Integer> idReserva;
    public static volatile CollectionAttribute<Reserva, Pago> pagoCollection;

}