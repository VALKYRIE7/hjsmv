package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Reserva;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile SingularAttribute<Usuario, String> usrNombre;
    public static volatile SingularAttribute<Usuario, String> usrCorreo;
    public static volatile SingularAttribute<Usuario, String> usrTipousuario;
    public static volatile SingularAttribute<Usuario, String> usrApellido;
    public static volatile SingularAttribute<Usuario, Date> usrFechanacimiento;
    public static volatile SingularAttribute<Usuario, Integer> idUsuario;
    public static volatile SingularAttribute<Usuario, Integer> usrIdentificacion;
    public static volatile SingularAttribute<Usuario, String> usrTipoidentificacion;
    public static volatile CollectionAttribute<Usuario, Reserva> reservaCollection;
    public static volatile SingularAttribute<Usuario, String> usrPassword;

}