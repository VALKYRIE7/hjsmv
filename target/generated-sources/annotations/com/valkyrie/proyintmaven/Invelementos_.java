package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Invelementos;
import com.valkyrie.proyintmaven.Inventario;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Invelementos.class)
public class Invelementos_ { 

    public static volatile SingularAttribute<Invelementos, Invelementos> invelementosCodsubtipo;
    public static volatile SingularAttribute<Invelementos, String> ivelNombre;
    public static volatile SingularAttribute<Invelementos, Integer> idElemento;
    public static volatile CollectionAttribute<Invelementos, Invelementos> invelementosCollection;
    public static volatile CollectionAttribute<Invelementos, Inventario> inventarioCollection;

}