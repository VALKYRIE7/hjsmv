package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Inmueble;
import com.valkyrie.proyintmaven.Invelementos;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Inventario.class)
public class Inventario_ { 

    public static volatile SingularAttribute<Inventario, String> invtNombre;
    public static volatile SingularAttribute<Inventario, Date> invtFechacompra;
    public static volatile SingularAttribute<Inventario, BigDecimal> invtCosto;
    public static volatile SingularAttribute<Inventario, Integer> invtCantidad;
    public static volatile SingularAttribute<Inventario, Invelementos> invelementosIdElemento;
    public static volatile SingularAttribute<Inventario, Integer> invtSerial;
    public static volatile SingularAttribute<Inventario, String> invtDescripcion;
    public static volatile SingularAttribute<Inventario, Integer> idInventario;
    public static volatile SingularAttribute<Inventario, Inmueble> inmueblesIdInmueble;

}