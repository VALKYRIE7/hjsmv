package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Empresa;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Identificacion.class)
public class Identificacion_ { 

    public static volatile SingularAttribute<Identificacion, Integer> idntNumero;
    public static volatile SingularAttribute<Identificacion, Empresa> empresasIdEmpresa;
    public static volatile SingularAttribute<Identificacion, Integer> idIdentificacion;
    public static volatile SingularAttribute<Identificacion, String> idntTipo;

}