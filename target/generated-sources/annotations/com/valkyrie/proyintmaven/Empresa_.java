package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Identificacion;
import com.valkyrie.proyintmaven.Inmueble;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Empresa.class)
public class Empresa_ { 

    public static volatile SingularAttribute<Empresa, Integer> emprWhatsapp;
    public static volatile CollectionAttribute<Empresa, Inmueble> inmuebleCollection;
    public static volatile SingularAttribute<Empresa, String> emprNombre;
    public static volatile SingularAttribute<Empresa, Integer> emprFax;
    public static volatile CollectionAttribute<Empresa, Identificacion> identificacionCollection;
    public static volatile SingularAttribute<Empresa, Integer> idEmpresa;
    public static volatile SingularAttribute<Empresa, Integer> emprTelefono;
    public static volatile SingularAttribute<Empresa, String> emprSkype;
    public static volatile SingularAttribute<Empresa, String> emprCorreo;

}