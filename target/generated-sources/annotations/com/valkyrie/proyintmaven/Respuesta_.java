package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Pregunta;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Respuesta.class)
public class Respuesta_ { 

    public static volatile SingularAttribute<Respuesta, String> rptaTexto;
    public static volatile SingularAttribute<Respuesta, Pregunta> preguntasIdPregunta;
    public static volatile SingularAttribute<Respuesta, Date> rptaFecha;
    public static volatile SingularAttribute<Respuesta, Integer> idRespuesta;

}