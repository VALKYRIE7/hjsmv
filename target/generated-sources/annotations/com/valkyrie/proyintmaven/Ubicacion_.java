package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Inmueble;
import com.valkyrie.proyintmaven.Lugar;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Ubicacion.class)
public class Ubicacion_ { 

    public static volatile CollectionAttribute<Ubicacion, Inmueble> inmuebleCollection;
    public static volatile SingularAttribute<Ubicacion, Lugar> lugaresLugrCodigolugar;
    public static volatile SingularAttribute<Ubicacion, String> ubicDireccion;
    public static volatile SingularAttribute<Ubicacion, Integer> idUbicacion;
    public static volatile SingularAttribute<Ubicacion, Float> ubicLatitud;
    public static volatile SingularAttribute<Ubicacion, Float> ubicLongitud;

}