package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Reserva;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Pago.class)
public class Pago_ { 

    public static volatile SingularAttribute<Pago, Integer> idPago;
    public static volatile SingularAttribute<Pago, BigDecimal> pagoCantidad;
    public static volatile SingularAttribute<Pago, Reserva> reservasIdReserva;
    public static volatile SingularAttribute<Pago, Date> pagoFecha;

}