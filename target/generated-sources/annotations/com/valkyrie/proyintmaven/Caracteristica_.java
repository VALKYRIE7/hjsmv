package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Inmueble;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Caracteristica.class)
public class Caracteristica_ { 

    public static volatile CollectionAttribute<Caracteristica, Inmueble> inmuebleCollection;
    public static volatile SingularAttribute<Caracteristica, Integer> idCaracteristica;
    public static volatile SingularAttribute<Caracteristica, String> crctNombre;
    public static volatile SingularAttribute<Caracteristica, String> crctDisponible;

}