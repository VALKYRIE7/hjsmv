package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Caracteristica;
import com.valkyrie.proyintmaven.Empresa;
import com.valkyrie.proyintmaven.Foto;
import com.valkyrie.proyintmaven.Inventario;
import com.valkyrie.proyintmaven.Pregunta;
import com.valkyrie.proyintmaven.Reserva;
import com.valkyrie.proyintmaven.Ubicacion;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Inmueble.class)
public class Inmueble_ { 

    public static volatile SingularAttribute<Inmueble, Integer> idInmueble;
    public static volatile SingularAttribute<Inmueble, BigDecimal> inmbPrecio;
    public static volatile SingularAttribute<Inmueble, Empresa> empresasIdEmpresa;
    public static volatile SingularAttribute<Inmueble, Integer> inmbAreainmb;
    public static volatile CollectionAttribute<Inmueble, Inventario> inventarioCollection;
    public static volatile CollectionAttribute<Inmueble, Pregunta> preguntaCollection;
    public static volatile CollectionAttribute<Inmueble, Caracteristica> caracteristicaCollection;
    public static volatile SingularAttribute<Inmueble, String> inmbNombre;
    public static volatile SingularAttribute<Inmueble, Ubicacion> ubicacionesIdUbicacion;
    public static volatile SingularAttribute<Inmueble, Integer> inmbBano;
    public static volatile CollectionAttribute<Inmueble, Reserva> reservaCollection;
    public static volatile CollectionAttribute<Inmueble, Foto> fotoCollection;
    public static volatile SingularAttribute<Inmueble, String> inmbDescripcion;

}