package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Inmueble;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Foto.class)
public class Foto_ { 

    public static volatile SingularAttribute<Foto, Integer> idFoto;
    public static volatile SingularAttribute<Foto, String> fotoUrl;
    public static volatile CollectionAttribute<Foto, Inmueble> inmuebleCollection;

}