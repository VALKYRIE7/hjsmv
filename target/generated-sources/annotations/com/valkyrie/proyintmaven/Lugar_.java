package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Lugar;
import com.valkyrie.proyintmaven.Ubicacion;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Lugar.class)
public class Lugar_ { 

    public static volatile SingularAttribute<Lugar, String> lugrNombrelugar;
    public static volatile SingularAttribute<Lugar, Lugar> lugaresCodigoubicacion;
    public static volatile CollectionAttribute<Lugar, Ubicacion> ubicacionCollection;
    public static volatile CollectionAttribute<Lugar, Lugar> lugarCollection;
    public static volatile SingularAttribute<Lugar, Character> lugrTipolugar;
    public static volatile SingularAttribute<Lugar, Integer> lugrCodigolugar;

}