package com.valkyrie.proyintmaven;

import com.valkyrie.proyintmaven.Inmueble;
import com.valkyrie.proyintmaven.Respuesta;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-23T23:27:25")
@StaticMetamodel(Pregunta.class)
public class Pregunta_ { 

    public static volatile CollectionAttribute<Pregunta, Respuesta> respuestaCollection;
    public static volatile SingularAttribute<Pregunta, Date> pgtaFecha;
    public static volatile SingularAttribute<Pregunta, Inmueble> inmueblesIdInmueble;
    public static volatile SingularAttribute<Pregunta, Integer> idPregunta;
    public static volatile SingularAttribute<Pregunta, String> pgtaTexto;

}