package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Respuesta;
import com.valkyrie.facade.RespuestaFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "respuestaController")
@ViewScoped
public class RespuestaController extends AbstractController<Respuesta> {

    @Inject
    private PreguntaController preguntasIdPreguntaController;
    @Inject
    private MobilePageController mobilePageController;

    public RespuestaController() {
        // Inform the Abstract parent controller of the concrete Respuesta Entity
        super(Respuesta.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        preguntasIdPreguntaController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Pregunta controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void preparePreguntasIdPregunta(ActionEvent event) {
        Respuesta selected = this.getSelected();
        if (selected != null && preguntasIdPreguntaController.getSelected() == null) {
            preguntasIdPreguntaController.setSelected(selected.getPreguntasIdPregunta());
        }
    }

}
