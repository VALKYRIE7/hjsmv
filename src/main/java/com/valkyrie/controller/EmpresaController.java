package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Empresa;
import com.valkyrie.proyintmaven.Inmueble;
import com.valkyrie.proyintmaven.Identificacion;
import java.util.Collection;
import com.valkyrie.facade.EmpresaFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "empresaController")
@ViewScoped
public class EmpresaController extends AbstractController<Empresa> {

    @Inject
    private MobilePageController mobilePageController;

    // Flags to indicate if child collections are empty
    private boolean isInmuebleCollectionEmpty;
    private boolean isIdentificacionCollectionEmpty;

    public EmpresaController() {
        // Inform the Abstract parent controller of the concrete Empresa Entity
        super(Empresa.class);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsInmuebleCollectionEmpty();
        this.setIsIdentificacionCollectionEmpty();
    }

    public boolean getIsInmuebleCollectionEmpty() {
        return this.isInmuebleCollectionEmpty;
    }

    private void setIsInmuebleCollectionEmpty() {
        Empresa selected = this.getSelected();
        if (selected != null) {
            EmpresaFacade ejbFacade = (EmpresaFacade) this.getFacade();
            this.isInmuebleCollectionEmpty = ejbFacade.isInmuebleCollectionEmpty(selected);
        } else {
            this.isInmuebleCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Inmueble entities that
     * are retrieved from Empresa and returns the navigation outcome.
     *
     * @return navigation outcome for Inmueble page
     */
    public String navigateInmuebleCollection() {
        Empresa selected = this.getSelected();
        if (selected != null) {
            EmpresaFacade ejbFacade = (EmpresaFacade) this.getFacade();
            Collection<Inmueble> selectedInmuebleCollection = ejbFacade.findInmuebleCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Inmueble_items", selectedInmuebleCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/inmueble/index";
    }

    public boolean getIsIdentificacionCollectionEmpty() {
        return this.isIdentificacionCollectionEmpty;
    }

    private void setIsIdentificacionCollectionEmpty() {
        Empresa selected = this.getSelected();
        if (selected != null) {
            EmpresaFacade ejbFacade = (EmpresaFacade) this.getFacade();
            this.isIdentificacionCollectionEmpty = ejbFacade.isIdentificacionCollectionEmpty(selected);
        } else {
            this.isIdentificacionCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Identificacion entities
     * that are retrieved from Empresa and returns the navigation outcome.
     *
     * @return navigation outcome for Identificacion page
     */
    public String navigateIdentificacionCollection() {
        Empresa selected = this.getSelected();
        if (selected != null) {
            EmpresaFacade ejbFacade = (EmpresaFacade) this.getFacade();
            Collection<Identificacion> selectedIdentificacionCollection = ejbFacade.findIdentificacionCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Identificacion_items", selectedIdentificacionCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/identificacion/index";
    }

}
