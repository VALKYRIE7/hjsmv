package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Reserva;
import com.valkyrie.proyintmaven.Inmueble;
import com.valkyrie.proyintmaven.Usuario;
import com.valkyrie.proyintmaven.Pago;
import java.util.Collection;
import com.valkyrie.facade.ReservaFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "reservaController")
@ViewScoped
public class ReservaController extends AbstractController<Reserva> {

    @Inject
    private MobilePageController mobilePageController;

    // Flags to indicate if child collections are empty
    private boolean isInmuebleCollectionEmpty;
    private boolean isUsuarioCollectionEmpty;
    private boolean isPagoCollectionEmpty;

    public ReservaController() {
        // Inform the Abstract parent controller of the concrete Reserva Entity
        super(Reserva.class);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsInmuebleCollectionEmpty();
        this.setIsUsuarioCollectionEmpty();
        this.setIsPagoCollectionEmpty();
    }

    public boolean getIsInmuebleCollectionEmpty() {
        return this.isInmuebleCollectionEmpty;
    }

    private void setIsInmuebleCollectionEmpty() {
        Reserva selected = this.getSelected();
        if (selected != null) {
            ReservaFacade ejbFacade = (ReservaFacade) this.getFacade();
            this.isInmuebleCollectionEmpty = ejbFacade.isInmuebleCollectionEmpty(selected);
        } else {
            this.isInmuebleCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Inmueble entities that
     * are retrieved from Reserva and returns the navigation outcome.
     *
     * @return navigation outcome for Inmueble page
     */
    public String navigateInmuebleCollection() {
        Reserva selected = this.getSelected();
        if (selected != null) {
            ReservaFacade ejbFacade = (ReservaFacade) this.getFacade();
            Collection<Inmueble> selectedInmuebleCollection = ejbFacade.findInmuebleCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Inmueble_items", selectedInmuebleCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/inmueble/index";
    }

    public boolean getIsUsuarioCollectionEmpty() {
        return this.isUsuarioCollectionEmpty;
    }

    private void setIsUsuarioCollectionEmpty() {
        Reserva selected = this.getSelected();
        if (selected != null) {
            ReservaFacade ejbFacade = (ReservaFacade) this.getFacade();
            this.isUsuarioCollectionEmpty = (selected.getUsuarioCollection() == null || selected.getUsuarioCollection().isEmpty());
        } else {
            this.isUsuarioCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Usuario entities that are
     * retrieved from Reserva and returns the navigation outcome.
     *
     * @return navigation outcome for Usuario page
     */
    public String navigateUsuarioCollection() {
        Reserva selected = this.getSelected();
        if (selected != null) {
            ReservaFacade ejbFacade = (ReservaFacade) this.getFacade();
            // Note: UsuarioCollection has already been read as is initialized
            Collection<Usuario> selectedUsuarioCollection = selected.getUsuarioCollection();
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Usuario_items", selectedUsuarioCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/usuario/index";
    }

    public boolean getIsPagoCollectionEmpty() {
        return this.isPagoCollectionEmpty;
    }

    private void setIsPagoCollectionEmpty() {
        Reserva selected = this.getSelected();
        if (selected != null) {
            ReservaFacade ejbFacade = (ReservaFacade) this.getFacade();
            this.isPagoCollectionEmpty = ejbFacade.isPagoCollectionEmpty(selected);
        } else {
            this.isPagoCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Pago entities that are
     * retrieved from Reserva and returns the navigation outcome.
     *
     * @return navigation outcome for Pago page
     */
    public String navigatePagoCollection() {
        Reserva selected = this.getSelected();
        if (selected != null) {
            ReservaFacade ejbFacade = (ReservaFacade) this.getFacade();
            Collection<Pago> selectedPagoCollection = ejbFacade.findPagoCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Pago_items", selectedPagoCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/pago/index";
    }

}
