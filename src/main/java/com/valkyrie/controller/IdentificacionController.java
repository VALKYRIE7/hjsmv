package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Identificacion;
import com.valkyrie.facade.IdentificacionFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "identificacionController")
@ViewScoped
public class IdentificacionController extends AbstractController<Identificacion> {

    @Inject
    private EmpresaController empresasIdEmpresaController;
    @Inject
    private MobilePageController mobilePageController;

    public IdentificacionController() {
        // Inform the Abstract parent controller of the concrete Identificacion Entity
        super(Identificacion.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        empresasIdEmpresaController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Empresa controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareEmpresasIdEmpresa(ActionEvent event) {
        Identificacion selected = this.getSelected();
        if (selected != null && empresasIdEmpresaController.getSelected() == null) {
            empresasIdEmpresaController.setSelected(selected.getEmpresasIdEmpresa());
        }
    }

}
