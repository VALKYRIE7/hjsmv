package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Inmueble;
import com.valkyrie.proyintmaven.Foto;
import com.valkyrie.proyintmaven.Caracteristica;
import com.valkyrie.proyintmaven.Reserva;
import com.valkyrie.proyintmaven.Inventario;
import com.valkyrie.proyintmaven.Pregunta;
import java.util.Collection;
import com.valkyrie.facade.InmuebleFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "inmuebleController")
@ViewScoped
public class InmuebleController extends AbstractController<Inmueble> {

    @Inject
    private EmpresaController empresasIdEmpresaController;
    @Inject
    private UbicacionController ubicacionesIdUbicacionController;
    @Inject
    private MobilePageController mobilePageController;

    // Flags to indicate if child collections are empty
    private boolean isFotoCollectionEmpty;
    private boolean isCaracteristicaCollectionEmpty;
    private boolean isReservaCollectionEmpty;
    private boolean isInventarioCollectionEmpty;
    private boolean isPreguntaCollectionEmpty;

    public InmuebleController() {
        // Inform the Abstract parent controller of the concrete Inmueble Entity
        super(Inmueble.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        empresasIdEmpresaController.setSelected(null);
        ubicacionesIdUbicacionController.setSelected(null);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsFotoCollectionEmpty();
        this.setIsCaracteristicaCollectionEmpty();
        this.setIsReservaCollectionEmpty();
        this.setIsInventarioCollectionEmpty();
        this.setIsPreguntaCollectionEmpty();
    }

    public boolean getIsFotoCollectionEmpty() {
        return this.isFotoCollectionEmpty;
    }

    private void setIsFotoCollectionEmpty() {
        Inmueble selected = this.getSelected();
        if (selected != null) {
            InmuebleFacade ejbFacade = (InmuebleFacade) this.getFacade();
            this.isFotoCollectionEmpty = ejbFacade.isFotoCollectionEmpty(selected);
        } else {
            this.isFotoCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Foto entities that are
     * retrieved from Inmueble and returns the navigation outcome.
     *
     * @return navigation outcome for Foto page
     */
    public String navigateFotoCollection() {
        Inmueble selected = this.getSelected();
        if (selected != null) {
            InmuebleFacade ejbFacade = (InmuebleFacade) this.getFacade();
            Collection<Foto> selectedFotoCollection = ejbFacade.findFotoCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Foto_items", selectedFotoCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/foto/index";
    }

    public boolean getIsCaracteristicaCollectionEmpty() {
        return this.isCaracteristicaCollectionEmpty;
    }

    private void setIsCaracteristicaCollectionEmpty() {
        Inmueble selected = this.getSelected();
        if (selected != null) {
            InmuebleFacade ejbFacade = (InmuebleFacade) this.getFacade();
            this.isCaracteristicaCollectionEmpty = ejbFacade.isCaracteristicaCollectionEmpty(selected);
        } else {
            this.isCaracteristicaCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Caracteristica entities
     * that are retrieved from Inmueble and returns the navigation outcome.
     *
     * @return navigation outcome for Caracteristica page
     */
    public String navigateCaracteristicaCollection() {
        Inmueble selected = this.getSelected();
        if (selected != null) {
            InmuebleFacade ejbFacade = (InmuebleFacade) this.getFacade();
            Collection<Caracteristica> selectedCaracteristicaCollection = ejbFacade.findCaracteristicaCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Caracteristica_items", selectedCaracteristicaCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/caracteristica/index";
    }

    public boolean getIsReservaCollectionEmpty() {
        return this.isReservaCollectionEmpty;
    }

    private void setIsReservaCollectionEmpty() {
        Inmueble selected = this.getSelected();
        if (selected != null) {
            InmuebleFacade ejbFacade = (InmuebleFacade) this.getFacade();
            this.isReservaCollectionEmpty = (selected.getReservaCollection() == null || selected.getReservaCollection().isEmpty());
        } else {
            this.isReservaCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Reserva entities that are
     * retrieved from Inmueble and returns the navigation outcome.
     *
     * @return navigation outcome for Reserva page
     */
    public String navigateReservaCollection() {
        Inmueble selected = this.getSelected();
        if (selected != null) {
            InmuebleFacade ejbFacade = (InmuebleFacade) this.getFacade();
            // Note: ReservaCollection has already been read as is initialized
            Collection<Reserva> selectedReservaCollection = selected.getReservaCollection();
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Reserva_items", selectedReservaCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/reserva/index";
    }

    public boolean getIsInventarioCollectionEmpty() {
        return this.isInventarioCollectionEmpty;
    }

    private void setIsInventarioCollectionEmpty() {
        Inmueble selected = this.getSelected();
        if (selected != null) {
            InmuebleFacade ejbFacade = (InmuebleFacade) this.getFacade();
            this.isInventarioCollectionEmpty = ejbFacade.isInventarioCollectionEmpty(selected);
        } else {
            this.isInventarioCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Inventario entities that
     * are retrieved from Inmueble and returns the navigation outcome.
     *
     * @return navigation outcome for Inventario page
     */
    public String navigateInventarioCollection() {
        Inmueble selected = this.getSelected();
        if (selected != null) {
            InmuebleFacade ejbFacade = (InmuebleFacade) this.getFacade();
            Collection<Inventario> selectedInventarioCollection = ejbFacade.findInventarioCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Inventario_items", selectedInventarioCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/inventario/index";
    }

    /**
     * Sets the "selected" attribute of the Empresa controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareEmpresasIdEmpresa(ActionEvent event) {
        Inmueble selected = this.getSelected();
        if (selected != null && empresasIdEmpresaController.getSelected() == null) {
            empresasIdEmpresaController.setSelected(selected.getEmpresasIdEmpresa());
        }
    }

    /**
     * Sets the "selected" attribute of the Ubicacion controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareUbicacionesIdUbicacion(ActionEvent event) {
        Inmueble selected = this.getSelected();
        if (selected != null && ubicacionesIdUbicacionController.getSelected() == null) {
            ubicacionesIdUbicacionController.setSelected(selected.getUbicacionesIdUbicacion());
        }
    }

    public boolean getIsPreguntaCollectionEmpty() {
        return this.isPreguntaCollectionEmpty;
    }

    private void setIsPreguntaCollectionEmpty() {
        Inmueble selected = this.getSelected();
        if (selected != null) {
            InmuebleFacade ejbFacade = (InmuebleFacade) this.getFacade();
            this.isPreguntaCollectionEmpty = ejbFacade.isPreguntaCollectionEmpty(selected);
        } else {
            this.isPreguntaCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Pregunta entities that
     * are retrieved from Inmueble and returns the navigation outcome.
     *
     * @return navigation outcome for Pregunta page
     */
    public String navigatePreguntaCollection() {
        Inmueble selected = this.getSelected();
        if (selected != null) {
            InmuebleFacade ejbFacade = (InmuebleFacade) this.getFacade();
            Collection<Pregunta> selectedPreguntaCollection = ejbFacade.findPreguntaCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Pregunta_items", selectedPreguntaCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/pregunta/index";
    }

}
