package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Caracteristica;
import com.valkyrie.proyintmaven.Inmueble;
import java.util.Collection;
import com.valkyrie.facade.CaracteristicaFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "caracteristicaController")
@ViewScoped
public class CaracteristicaController extends AbstractController<Caracteristica> {

    @Inject
    private MobilePageController mobilePageController;

    // Flags to indicate if child collections are empty
    private boolean isInmuebleCollectionEmpty;

    public CaracteristicaController() {
        // Inform the Abstract parent controller of the concrete Caracteristica Entity
        super(Caracteristica.class);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsInmuebleCollectionEmpty();
    }

    public boolean getIsInmuebleCollectionEmpty() {
        return this.isInmuebleCollectionEmpty;
    }

    private void setIsInmuebleCollectionEmpty() {
        Caracteristica selected = this.getSelected();
        if (selected != null) {
            CaracteristicaFacade ejbFacade = (CaracteristicaFacade) this.getFacade();
            this.isInmuebleCollectionEmpty = (selected.getInmuebleCollection() == null || selected.getInmuebleCollection().isEmpty());
        } else {
            this.isInmuebleCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Inmueble entities that
     * are retrieved from Caracteristica and returns the navigation outcome.
     *
     * @return navigation outcome for Inmueble page
     */
    public String navigateInmuebleCollection() {
        Caracteristica selected = this.getSelected();
        if (selected != null) {
            CaracteristicaFacade ejbFacade = (CaracteristicaFacade) this.getFacade();
            // Note: InmuebleCollection has already been read as is initialized
            Collection<Inmueble> selectedInmuebleCollection = selected.getInmuebleCollection();
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Inmueble_items", selectedInmuebleCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/inmueble/index";
    }

}
