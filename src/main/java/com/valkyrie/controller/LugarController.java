package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Lugar;
import com.valkyrie.proyintmaven.Lugar;
import com.valkyrie.proyintmaven.Ubicacion;
import java.util.Collection;
import com.valkyrie.facade.LugarFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "lugarController")
@ViewScoped
public class LugarController extends AbstractController<Lugar> {

    @Inject
    private LugarController lugaresCodigoubicacionController;
    @Inject
    private MobilePageController mobilePageController;

    // Flags to indicate if child collections are empty
    private boolean isLugarCollectionEmpty;
    private boolean isUbicacionCollectionEmpty;

    public LugarController() {
        // Inform the Abstract parent controller of the concrete Lugar Entity
        super(Lugar.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        lugaresCodigoubicacionController.setSelected(null);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsLugarCollectionEmpty();
        this.setIsUbicacionCollectionEmpty();
    }

    public boolean getIsLugarCollectionEmpty() {
        return this.isLugarCollectionEmpty;
    }

    private void setIsLugarCollectionEmpty() {
        Lugar selected = this.getSelected();
        if (selected != null) {
            LugarFacade ejbFacade = (LugarFacade) this.getFacade();
            this.isLugarCollectionEmpty = ejbFacade.isLugarCollectionEmpty(selected);
        } else {
            this.isLugarCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Lugar entities that are
     * retrieved from Lugar and returns the navigation outcome.
     *
     * @return navigation outcome for Lugar page
     */
    public String navigateLugarCollection() {
        Lugar selected = this.getSelected();
        if (selected != null) {
            LugarFacade ejbFacade = (LugarFacade) this.getFacade();
            Collection<Lugar> selectedLugarCollection = ejbFacade.findLugarCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Lugar_items", selectedLugarCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/lugar/index";
    }

    /**
     * Sets the "selected" attribute of the Lugar controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareLugaresCodigoubicacion(ActionEvent event) {
        Lugar selected = this.getSelected();
        if (selected != null && lugaresCodigoubicacionController.getSelected() == null) {
            lugaresCodigoubicacionController.setSelected(selected.getLugaresCodigoubicacion());
        }
    }

    public boolean getIsUbicacionCollectionEmpty() {
        return this.isUbicacionCollectionEmpty;
    }

    private void setIsUbicacionCollectionEmpty() {
        Lugar selected = this.getSelected();
        if (selected != null) {
            LugarFacade ejbFacade = (LugarFacade) this.getFacade();
            this.isUbicacionCollectionEmpty = ejbFacade.isUbicacionCollectionEmpty(selected);
        } else {
            this.isUbicacionCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Ubicacion entities that
     * are retrieved from Lugar and returns the navigation outcome.
     *
     * @return navigation outcome for Ubicacion page
     */
    public String navigateUbicacionCollection() {
        Lugar selected = this.getSelected();
        if (selected != null) {
            LugarFacade ejbFacade = (LugarFacade) this.getFacade();
            Collection<Ubicacion> selectedUbicacionCollection = ejbFacade.findUbicacionCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Ubicacion_items", selectedUbicacionCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/ubicacion/index";
    }

}
