package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Inventario;
import com.valkyrie.facade.InventarioFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "inventarioController")
@ViewScoped
public class InventarioController extends AbstractController<Inventario> {

    @Inject
    private InmuebleController inmueblesIdInmuebleController;
    @Inject
    private InvelementosController invelementosIdElementoController;
    @Inject
    private MobilePageController mobilePageController;

    public InventarioController() {
        // Inform the Abstract parent controller of the concrete Inventario Entity
        super(Inventario.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        inmueblesIdInmuebleController.setSelected(null);
        invelementosIdElementoController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Inmueble controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareInmueblesIdInmueble(ActionEvent event) {
        Inventario selected = this.getSelected();
        if (selected != null && inmueblesIdInmuebleController.getSelected() == null) {
            inmueblesIdInmuebleController.setSelected(selected.getInmueblesIdInmueble());
        }
    }

    /**
     * Sets the "selected" attribute of the Invelementos controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareInvelementosIdElemento(ActionEvent event) {
        Inventario selected = this.getSelected();
        if (selected != null && invelementosIdElementoController.getSelected() == null) {
            invelementosIdElementoController.setSelected(selected.getInvelementosIdElemento());
        }
    }

}
