package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Invelementos;
import com.valkyrie.proyintmaven.Inventario;
import com.valkyrie.proyintmaven.Invelementos;
import java.util.Collection;
import com.valkyrie.facade.InvelementosFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "invelementosController")
@ViewScoped
public class InvelementosController extends AbstractController<Invelementos> {

    @Inject
    private InvelementosController invelementosCodsubtipoController;
    @Inject
    private MobilePageController mobilePageController;

    // Flags to indicate if child collections are empty
    private boolean isInventarioCollectionEmpty;
    private boolean isInvelementosCollectionEmpty;

    public InvelementosController() {
        // Inform the Abstract parent controller of the concrete Invelementos Entity
        super(Invelementos.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        invelementosCodsubtipoController.setSelected(null);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsInventarioCollectionEmpty();
        this.setIsInvelementosCollectionEmpty();
    }

    public boolean getIsInventarioCollectionEmpty() {
        return this.isInventarioCollectionEmpty;
    }

    private void setIsInventarioCollectionEmpty() {
        Invelementos selected = this.getSelected();
        if (selected != null) {
            InvelementosFacade ejbFacade = (InvelementosFacade) this.getFacade();
            this.isInventarioCollectionEmpty = ejbFacade.isInventarioCollectionEmpty(selected);
        } else {
            this.isInventarioCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Inventario entities that
     * are retrieved from Invelementos and returns the navigation outcome.
     *
     * @return navigation outcome for Inventario page
     */
    public String navigateInventarioCollection() {
        Invelementos selected = this.getSelected();
        if (selected != null) {
            InvelementosFacade ejbFacade = (InvelementosFacade) this.getFacade();
            Collection<Inventario> selectedInventarioCollection = ejbFacade.findInventarioCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Inventario_items", selectedInventarioCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/inventario/index";
    }

    public boolean getIsInvelementosCollectionEmpty() {
        return this.isInvelementosCollectionEmpty;
    }

    private void setIsInvelementosCollectionEmpty() {
        Invelementos selected = this.getSelected();
        if (selected != null) {
            InvelementosFacade ejbFacade = (InvelementosFacade) this.getFacade();
            this.isInvelementosCollectionEmpty = ejbFacade.isInvelementosCollectionEmpty(selected);
        } else {
            this.isInvelementosCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Invelementos entities
     * that are retrieved from Invelementos and returns the navigation outcome.
     *
     * @return navigation outcome for Invelementos page
     */
    public String navigateInvelementosCollection() {
        Invelementos selected = this.getSelected();
        if (selected != null) {
            InvelementosFacade ejbFacade = (InvelementosFacade) this.getFacade();
            Collection<Invelementos> selectedInvelementosCollection = ejbFacade.findInvelementosCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Invelementos_items", selectedInvelementosCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/invelementos/index";
    }

    /**
     * Sets the "selected" attribute of the Invelementos controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareInvelementosCodsubtipo(ActionEvent event) {
        Invelementos selected = this.getSelected();
        if (selected != null && invelementosCodsubtipoController.getSelected() == null) {
            invelementosCodsubtipoController.setSelected(selected.getInvelementosCodsubtipo());
        }
    }

}
