package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Pago;
import com.valkyrie.facade.PagoFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "pagoController")
@ViewScoped
public class PagoController extends AbstractController<Pago> {

    @Inject
    private ReservaController reservasIdReservaController;
    @Inject
    private MobilePageController mobilePageController;

    public PagoController() {
        // Inform the Abstract parent controller of the concrete Pago Entity
        super(Pago.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        reservasIdReservaController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Reserva controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareReservasIdReserva(ActionEvent event) {
        Pago selected = this.getSelected();
        if (selected != null && reservasIdReservaController.getSelected() == null) {
            reservasIdReservaController.setSelected(selected.getReservasIdReserva());
        }
    }

}
