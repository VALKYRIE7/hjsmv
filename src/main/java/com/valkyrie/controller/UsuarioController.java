package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Usuario;
import com.valkyrie.proyintmaven.Reserva;
import java.util.Collection;
import com.valkyrie.facade.UsuarioFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@Named(value = "usuarioController")
@ViewScoped
public class UsuarioController extends AbstractController<Usuario> {

    @Inject
    private MobilePageController mobilePageController;

    // Flags to indicate if child collections are empty
    private boolean isReservaCollectionEmpty;

    public UsuarioController() {
        // Inform the Abstract parent controller of the concrete Usuario Entity
        super(Usuario.class);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsReservaCollectionEmpty();
    }

    public boolean getIsReservaCollectionEmpty() {
        return this.isReservaCollectionEmpty;
    }

    private void setIsReservaCollectionEmpty() {
        Usuario selected = this.getSelected();
        if (selected != null) {
            UsuarioFacade ejbFacade = (UsuarioFacade) this.getFacade();
            this.isReservaCollectionEmpty = ejbFacade.isReservaCollectionEmpty(selected);
        } else {
            this.isReservaCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Reserva entities that are
     * retrieved from Usuario and returns the navigation outcome.
     *
     * @return navigation outcome for Reserva page
     */
    public String navigateReservaCollection() {
        Usuario selected = this.getSelected();
        if (selected != null) {
            UsuarioFacade ejbFacade = (UsuarioFacade) this.getFacade();
            Collection<Reserva> selectedReservaCollection = ejbFacade.findReservaCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Reserva_items", selectedReservaCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/reserva/index";
    }

}
