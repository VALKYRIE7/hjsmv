package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Ubicacion;
import com.valkyrie.proyintmaven.Inmueble;
import java.util.Collection;
import com.valkyrie.facade.UbicacionFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "ubicacionController")
@ViewScoped
public class UbicacionController extends AbstractController<Ubicacion> {

    @Inject
    private LugarController lugaresLugrCodigolugarController;
    @Inject
    private MobilePageController mobilePageController;

    // Flags to indicate if child collections are empty
    private boolean isInmuebleCollectionEmpty;

    public UbicacionController() {
        // Inform the Abstract parent controller of the concrete Ubicacion Entity
        super(Ubicacion.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        lugaresLugrCodigolugarController.setSelected(null);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsInmuebleCollectionEmpty();
    }

    public boolean getIsInmuebleCollectionEmpty() {
        return this.isInmuebleCollectionEmpty;
    }

    private void setIsInmuebleCollectionEmpty() {
        Ubicacion selected = this.getSelected();
        if (selected != null) {
            UbicacionFacade ejbFacade = (UbicacionFacade) this.getFacade();
            this.isInmuebleCollectionEmpty = ejbFacade.isInmuebleCollectionEmpty(selected);
        } else {
            this.isInmuebleCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Inmueble entities that
     * are retrieved from Ubicacion and returns the navigation outcome.
     *
     * @return navigation outcome for Inmueble page
     */
    public String navigateInmuebleCollection() {
        Ubicacion selected = this.getSelected();
        if (selected != null) {
            UbicacionFacade ejbFacade = (UbicacionFacade) this.getFacade();
            Collection<Inmueble> selectedInmuebleCollection = ejbFacade.findInmuebleCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Inmueble_items", selectedInmuebleCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/inmueble/index";
    }

    /**
     * Sets the "selected" attribute of the Lugar controller in order to display
     * its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareLugaresLugrCodigolugar(ActionEvent event) {
        Ubicacion selected = this.getSelected();
        if (selected != null && lugaresLugrCodigolugarController.getSelected() == null) {
            lugaresLugrCodigolugarController.setSelected(selected.getLugaresLugrCodigolugar());
        }
    }

}
