package com.valkyrie.controller;

import com.valkyrie.proyintmaven.Pregunta;
import com.valkyrie.proyintmaven.Respuesta;
import java.util.Collection;
import com.valkyrie.facade.PreguntaFacade;
import com.valkyrie.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "preguntaController")
@ViewScoped
public class PreguntaController extends AbstractController<Pregunta> {

    @Inject
    private InmuebleController inmueblesIdInmuebleController;
    @Inject
    private MobilePageController mobilePageController;

    // Flags to indicate if child collections are empty
    private boolean isRespuestaCollectionEmpty;

    public PreguntaController() {
        // Inform the Abstract parent controller of the concrete Pregunta Entity
        super(Pregunta.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        inmueblesIdInmuebleController.setSelected(null);
    }

    /**
     * Set the "is[ChildCollection]Empty" property for OneToMany fields.
     */
    @Override
    protected void setChildrenEmptyFlags() {
        this.setIsRespuestaCollectionEmpty();
    }

    public boolean getIsRespuestaCollectionEmpty() {
        return this.isRespuestaCollectionEmpty;
    }

    private void setIsRespuestaCollectionEmpty() {
        Pregunta selected = this.getSelected();
        if (selected != null) {
            PreguntaFacade ejbFacade = (PreguntaFacade) this.getFacade();
            this.isRespuestaCollectionEmpty = ejbFacade.isRespuestaCollectionEmpty(selected);
        } else {
            this.isRespuestaCollectionEmpty = true;
        }
    }

    /**
     * Sets the "items" attribute with a collection of Respuesta entities that
     * are retrieved from Pregunta and returns the navigation outcome.
     *
     * @return navigation outcome for Respuesta page
     */
    public String navigateRespuestaCollection() {
        Pregunta selected = this.getSelected();
        if (selected != null) {
            PreguntaFacade ejbFacade = (PreguntaFacade) this.getFacade();
            Collection<Respuesta> selectedRespuestaCollection = ejbFacade.findRespuestaCollection(selected);
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Respuesta_items", selectedRespuestaCollection);
        }
        return this.mobilePageController.getMobilePagesPrefix() + "/app/respuesta/index";
    }

    /**
     * Sets the "selected" attribute of the Inmueble controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareInmueblesIdInmueble(ActionEvent event) {
        Pregunta selected = this.getSelected();
        if (selected != null && inmueblesIdInmuebleController.getSelected() == null) {
            inmueblesIdInmuebleController.setSelected(selected.getInmueblesIdInmueble());
        }
    }

}
