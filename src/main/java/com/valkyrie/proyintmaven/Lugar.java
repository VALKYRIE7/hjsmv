/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "lugares")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lugar.findAll", query = "SELECT l FROM Lugar l")
    , @NamedQuery(name = "Lugar.findByLugrCodigolugar", query = "SELECT l FROM Lugar l WHERE l.lugrCodigolugar = :lugrCodigolugar")
    , @NamedQuery(name = "Lugar.findByLugrNombrelugar", query = "SELECT l FROM Lugar l WHERE l.lugrNombrelugar = :lugrNombrelugar")
    , @NamedQuery(name = "Lugar.findByLugrTipolugar", query = "SELECT l FROM Lugar l WHERE l.lugrTipolugar = :lugrTipolugar")})
public class Lugar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "lugr_codigolugar")
    private Integer lugrCodigolugar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "lugr_nombrelugar")
    private String lugrNombrelugar;
    @Basic(optional = false)
    @NotNull
    @Column(name = "lugr_tipolugar")
    private Character lugrTipolugar;
    @OneToMany(mappedBy = "lugaresCodigoubicacion")
    private Collection<Lugar> lugarCollection;
    @JoinColumn(name = "lugares_codigoubicacion", referencedColumnName = "lugr_codigolugar")
    @ManyToOne
    private Lugar lugaresCodigoubicacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lugaresLugrCodigolugar")
    private Collection<Ubicacion> ubicacionCollection;

    public Lugar() {
    }

    public Lugar(Integer lugrCodigolugar) {
        this.lugrCodigolugar = lugrCodigolugar;
    }

    public Lugar(Integer lugrCodigolugar, String lugrNombrelugar, Character lugrTipolugar) {
        this.lugrCodigolugar = lugrCodigolugar;
        this.lugrNombrelugar = lugrNombrelugar;
        this.lugrTipolugar = lugrTipolugar;
    }

    public Integer getLugrCodigolugar() {
        return lugrCodigolugar;
    }

    public void setLugrCodigolugar(Integer lugrCodigolugar) {
        this.lugrCodigolugar = lugrCodigolugar;
    }

    public String getLugrNombrelugar() {
        return lugrNombrelugar;
    }

    public void setLugrNombrelugar(String lugrNombrelugar) {
        this.lugrNombrelugar = lugrNombrelugar;
    }

    public Character getLugrTipolugar() {
        return lugrTipolugar;
    }

    public void setLugrTipolugar(Character lugrTipolugar) {
        this.lugrTipolugar = lugrTipolugar;
    }

    @XmlTransient
    public Collection<Lugar> getLugarCollection() {
        return lugarCollection;
    }

    public void setLugarCollection(Collection<Lugar> lugarCollection) {
        this.lugarCollection = lugarCollection;
    }

    public Lugar getLugaresCodigoubicacion() {
        return lugaresCodigoubicacion;
    }

    public void setLugaresCodigoubicacion(Lugar lugaresCodigoubicacion) {
        this.lugaresCodigoubicacion = lugaresCodigoubicacion;
    }

    @XmlTransient
    public Collection<Ubicacion> getUbicacionCollection() {
        return ubicacionCollection;
    }

    public void setUbicacionCollection(Collection<Ubicacion> ubicacionCollection) {
        this.ubicacionCollection = ubicacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lugrCodigolugar != null ? lugrCodigolugar.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lugar)) {
            return false;
        }
        Lugar other = (Lugar) object;
        if ((this.lugrCodigolugar == null && other.lugrCodigolugar != null) || (this.lugrCodigolugar != null && !this.lugrCodigolugar.equals(other.lugrCodigolugar))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Lugar[ lugrCodigolugar=" + lugrCodigolugar + " ]";
    }
    
}
