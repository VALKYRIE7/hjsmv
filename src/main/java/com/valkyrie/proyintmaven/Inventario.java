/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "inventarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inventario.findAll", query = "SELECT i FROM Inventario i")
    , @NamedQuery(name = "Inventario.findByIdInventario", query = "SELECT i FROM Inventario i WHERE i.idInventario = :idInventario")
    , @NamedQuery(name = "Inventario.findByInvtSerial", query = "SELECT i FROM Inventario i WHERE i.invtSerial = :invtSerial")
    , @NamedQuery(name = "Inventario.findByInvtNombre", query = "SELECT i FROM Inventario i WHERE i.invtNombre = :invtNombre")
    , @NamedQuery(name = "Inventario.findByInvtCosto", query = "SELECT i FROM Inventario i WHERE i.invtCosto = :invtCosto")
    , @NamedQuery(name = "Inventario.findByInvtDescripcion", query = "SELECT i FROM Inventario i WHERE i.invtDescripcion = :invtDescripcion")
    , @NamedQuery(name = "Inventario.findByInvtCantidad", query = "SELECT i FROM Inventario i WHERE i.invtCantidad = :invtCantidad")
    , @NamedQuery(name = "Inventario.findByInvtFechacompra", query = "SELECT i FROM Inventario i WHERE i.invtFechacompra = :invtFechacompra")})
public class Inventario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_inventario")
    private Integer idInventario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "invt_serial")
    private int invtSerial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "invt_nombre")
    private String invtNombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "invt_costo")
    private BigDecimal invtCosto;
    @Size(max = 1000)
    @Column(name = "invt_descripcion")
    private String invtDescripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "invt_cantidad")
    private int invtCantidad;
    @Column(name = "invt_fechacompra")
    @Temporal(TemporalType.DATE)
    private Date invtFechacompra;
    @JoinColumn(name = "inmuebles_id_inmueble", referencedColumnName = "id_inmueble")
    @ManyToOne(optional = false)
    private Inmueble inmueblesIdInmueble;
    @JoinColumn(name = "invelementos_id_elemento", referencedColumnName = "id_elemento")
    @ManyToOne(optional = false)
    private Invelementos invelementosIdElemento;

    public Inventario() {
    }

    public Inventario(Integer idInventario) {
        this.idInventario = idInventario;
    }

    public Inventario(Integer idInventario, int invtSerial, String invtNombre, BigDecimal invtCosto, int invtCantidad) {
        this.idInventario = idInventario;
        this.invtSerial = invtSerial;
        this.invtNombre = invtNombre;
        this.invtCosto = invtCosto;
        this.invtCantidad = invtCantidad;
    }

    public Integer getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(Integer idInventario) {
        this.idInventario = idInventario;
    }

    public int getInvtSerial() {
        return invtSerial;
    }

    public void setInvtSerial(int invtSerial) {
        this.invtSerial = invtSerial;
    }

    public String getInvtNombre() {
        return invtNombre;
    }

    public void setInvtNombre(String invtNombre) {
        this.invtNombre = invtNombre;
    }

    public BigDecimal getInvtCosto() {
        return invtCosto;
    }

    public void setInvtCosto(BigDecimal invtCosto) {
        this.invtCosto = invtCosto;
    }

    public String getInvtDescripcion() {
        return invtDescripcion;
    }

    public void setInvtDescripcion(String invtDescripcion) {
        this.invtDescripcion = invtDescripcion;
    }

    public int getInvtCantidad() {
        return invtCantidad;
    }

    public void setInvtCantidad(int invtCantidad) {
        this.invtCantidad = invtCantidad;
    }

    public Date getInvtFechacompra() {
        return invtFechacompra;
    }

    public void setInvtFechacompra(Date invtFechacompra) {
        this.invtFechacompra = invtFechacompra;
    }

    public Inmueble getInmueblesIdInmueble() {
        return inmueblesIdInmueble;
    }

    public void setInmueblesIdInmueble(Inmueble inmueblesIdInmueble) {
        this.inmueblesIdInmueble = inmueblesIdInmueble;
    }

    public Invelementos getInvelementosIdElemento() {
        return invelementosIdElemento;
    }

    public void setInvelementosIdElemento(Invelementos invelementosIdElemento) {
        this.invelementosIdElemento = invelementosIdElemento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInventario != null ? idInventario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inventario)) {
            return false;
        }
        Inventario other = (Inventario) object;
        if ((this.idInventario == null && other.idInventario != null) || (this.idInventario != null && !this.idInventario.equals(other.idInventario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Inventario[ idInventario=" + idInventario + " ]";
    }
    
}
