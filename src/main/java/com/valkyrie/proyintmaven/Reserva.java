/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "reservas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reserva.findAll", query = "SELECT r FROM Reserva r")
    , @NamedQuery(name = "Reserva.findByIdReserva", query = "SELECT r FROM Reserva r WHERE r.idReserva = :idReserva")
    , @NamedQuery(name = "Reserva.findByRsvaFecharealizada", query = "SELECT r FROM Reserva r WHERE r.rsvaFecharealizada = :rsvaFecharealizada")
    , @NamedQuery(name = "Reserva.findByRsvaFechallegada", query = "SELECT r FROM Reserva r WHERE r.rsvaFechallegada = :rsvaFechallegada")
    , @NamedQuery(name = "Reserva.findByRsvaCheckin", query = "SELECT r FROM Reserva r WHERE r.rsvaCheckin = :rsvaCheckin")
    , @NamedQuery(name = "Reserva.findByRsvaCheckout", query = "SELECT r FROM Reserva r WHERE r.rsvaCheckout = :rsvaCheckout")
    , @NamedQuery(name = "Reserva.findByRsvaNoche", query = "SELECT r FROM Reserva r WHERE r.rsvaNoche = :rsvaNoche")
    , @NamedQuery(name = "Reserva.findByRsvaAdulto", query = "SELECT r FROM Reserva r WHERE r.rsvaAdulto = :rsvaAdulto")
    , @NamedQuery(name = "Reserva.findByRsvaMenoredad", query = "SELECT r FROM Reserva r WHERE r.rsvaMenoredad = :rsvaMenoredad")
    , @NamedQuery(name = "Reserva.findByRsvaEstado", query = "SELECT r FROM Reserva r WHERE r.rsvaEstado = :rsvaEstado")})
public class Reserva implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_reserva")
    private Integer idReserva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rsva_fecharealizada")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rsvaFecharealizada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rsva_fechallegada")
    private int rsvaFechallegada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rsva_checkin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rsvaCheckin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rsva_checkout")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rsvaCheckout;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rsva_noche")
    private int rsvaNoche;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rsva_adulto")
    private int rsvaAdulto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rsva_menoredad")
    private int rsvaMenoredad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "rsva_estado")
    private String rsvaEstado;
    @ManyToMany(mappedBy = "reservaCollection")
    private Collection<Inmueble> inmuebleCollection;
    @JoinTable(name = "reservasusuarios", joinColumns = {
        @JoinColumn(name = "reservas_id_reserva", referencedColumnName = "id_reserva")}, inverseJoinColumns = {
        @JoinColumn(name = "usuarios_id_usuario", referencedColumnName = "id_usuario")})
    @ManyToMany
    private Collection<Usuario> usuarioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reservasIdReserva")
    private Collection<Pago> pagoCollection;

    public Reserva() {
    }

    public Reserva(Integer idReserva) {
        this.idReserva = idReserva;
    }

    public Reserva(Integer idReserva, Date rsvaFecharealizada, int rsvaFechallegada, Date rsvaCheckin, Date rsvaCheckout, int rsvaNoche, int rsvaAdulto, int rsvaMenoredad, String rsvaEstado) {
        this.idReserva = idReserva;
        this.rsvaFecharealizada = rsvaFecharealizada;
        this.rsvaFechallegada = rsvaFechallegada;
        this.rsvaCheckin = rsvaCheckin;
        this.rsvaCheckout = rsvaCheckout;
        this.rsvaNoche = rsvaNoche;
        this.rsvaAdulto = rsvaAdulto;
        this.rsvaMenoredad = rsvaMenoredad;
        this.rsvaEstado = rsvaEstado;
    }

    public Integer getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
    }

    public Date getRsvaFecharealizada() {
        return rsvaFecharealizada;
    }

    public void setRsvaFecharealizada(Date rsvaFecharealizada) {
        this.rsvaFecharealizada = rsvaFecharealizada;
    }

    public int getRsvaFechallegada() {
        return rsvaFechallegada;
    }

    public void setRsvaFechallegada(int rsvaFechallegada) {
        this.rsvaFechallegada = rsvaFechallegada;
    }

    public Date getRsvaCheckin() {
        return rsvaCheckin;
    }

    public void setRsvaCheckin(Date rsvaCheckin) {
        this.rsvaCheckin = rsvaCheckin;
    }

    public Date getRsvaCheckout() {
        return rsvaCheckout;
    }

    public void setRsvaCheckout(Date rsvaCheckout) {
        this.rsvaCheckout = rsvaCheckout;
    }

    public int getRsvaNoche() {
        return rsvaNoche;
    }

    public void setRsvaNoche(int rsvaNoche) {
        this.rsvaNoche = rsvaNoche;
    }

    public int getRsvaAdulto() {
        return rsvaAdulto;
    }

    public void setRsvaAdulto(int rsvaAdulto) {
        this.rsvaAdulto = rsvaAdulto;
    }

    public int getRsvaMenoredad() {
        return rsvaMenoredad;
    }

    public void setRsvaMenoredad(int rsvaMenoredad) {
        this.rsvaMenoredad = rsvaMenoredad;
    }

    public String getRsvaEstado() {
        return rsvaEstado;
    }

    public void setRsvaEstado(String rsvaEstado) {
        this.rsvaEstado = rsvaEstado;
    }

    @XmlTransient
    public Collection<Inmueble> getInmuebleCollection() {
        return inmuebleCollection;
    }

    public void setInmuebleCollection(Collection<Inmueble> inmuebleCollection) {
        this.inmuebleCollection = inmuebleCollection;
    }

    @XmlTransient
    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }

    @XmlTransient
    public Collection<Pago> getPagoCollection() {
        return pagoCollection;
    }

    public void setPagoCollection(Collection<Pago> pagoCollection) {
        this.pagoCollection = pagoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReserva != null ? idReserva.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reserva)) {
            return false;
        }
        Reserva other = (Reserva) object;
        if ((this.idReserva == null && other.idReserva != null) || (this.idReserva != null && !this.idReserva.equals(other.idReserva))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Reserva[ idReserva=" + idReserva + " ]";
    }
    
}
