/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "invelementos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Invelementos.findAll", query = "SELECT i FROM Invelementos i")
    , @NamedQuery(name = "Invelementos.findByIdElemento", query = "SELECT i FROM Invelementos i WHERE i.idElemento = :idElemento")
    , @NamedQuery(name = "Invelementos.findByIvelNombre", query = "SELECT i FROM Invelementos i WHERE i.ivelNombre = :ivelNombre")})
public class Invelementos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_elemento")
    private Integer idElemento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "ivel_nombre")
    private String ivelNombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invelementosIdElemento")
    private Collection<Inventario> inventarioCollection;
    @OneToMany(mappedBy = "invelementosCodsubtipo")
    private Collection<Invelementos> invelementosCollection;
    @JoinColumn(name = "invelementos_codsubtipo", referencedColumnName = "id_elemento")
    @ManyToOne
    private Invelementos invelementosCodsubtipo;

    public Invelementos() {
    }

    public Invelementos(Integer idElemento) {
        this.idElemento = idElemento;
    }

    public Invelementos(Integer idElemento, String ivelNombre) {
        this.idElemento = idElemento;
        this.ivelNombre = ivelNombre;
    }

    public Integer getIdElemento() {
        return idElemento;
    }

    public void setIdElemento(Integer idElemento) {
        this.idElemento = idElemento;
    }

    public String getIvelNombre() {
        return ivelNombre;
    }

    public void setIvelNombre(String ivelNombre) {
        this.ivelNombre = ivelNombre;
    }

    @XmlTransient
    public Collection<Inventario> getInventarioCollection() {
        return inventarioCollection;
    }

    public void setInventarioCollection(Collection<Inventario> inventarioCollection) {
        this.inventarioCollection = inventarioCollection;
    }

    @XmlTransient
    public Collection<Invelementos> getInvelementosCollection() {
        return invelementosCollection;
    }

    public void setInvelementosCollection(Collection<Invelementos> invelementosCollection) {
        this.invelementosCollection = invelementosCollection;
    }

    public Invelementos getInvelementosCodsubtipo() {
        return invelementosCodsubtipo;
    }

    public void setInvelementosCodsubtipo(Invelementos invelementosCodsubtipo) {
        this.invelementosCodsubtipo = invelementosCodsubtipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idElemento != null ? idElemento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Invelementos)) {
            return false;
        }
        Invelementos other = (Invelementos) object;
        if ((this.idElemento == null && other.idElemento != null) || (this.idElemento != null && !this.idElemento.equals(other.idElemento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Invelementos[ idElemento=" + idElemento + " ]";
    }
    
}
