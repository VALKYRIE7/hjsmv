/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "respuestas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Respuesta.findAll", query = "SELECT r FROM Respuesta r")
    , @NamedQuery(name = "Respuesta.findByIdRespuesta", query = "SELECT r FROM Respuesta r WHERE r.idRespuesta = :idRespuesta")
    , @NamedQuery(name = "Respuesta.findByRptaTexto", query = "SELECT r FROM Respuesta r WHERE r.rptaTexto = :rptaTexto")
    , @NamedQuery(name = "Respuesta.findByRptaFecha", query = "SELECT r FROM Respuesta r WHERE r.rptaFecha = :rptaFecha")})
public class Respuesta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_respuesta")
    private Integer idRespuesta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "rpta_texto")
    private String rptaTexto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rpta_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rptaFecha;
    @JoinColumn(name = "preguntas_id_pregunta", referencedColumnName = "id_pregunta")
    @ManyToOne(optional = false)
    private Pregunta preguntasIdPregunta;

    public Respuesta() {
    }

    public Respuesta(Integer idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public Respuesta(Integer idRespuesta, String rptaTexto, Date rptaFecha) {
        this.idRespuesta = idRespuesta;
        this.rptaTexto = rptaTexto;
        this.rptaFecha = rptaFecha;
    }

    public Integer getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(Integer idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public String getRptaTexto() {
        return rptaTexto;
    }

    public void setRptaTexto(String rptaTexto) {
        this.rptaTexto = rptaTexto;
    }

    public Date getRptaFecha() {
        return rptaFecha;
    }

    public void setRptaFecha(Date rptaFecha) {
        this.rptaFecha = rptaFecha;
    }

    public Pregunta getPreguntasIdPregunta() {
        return preguntasIdPregunta;
    }

    public void setPreguntasIdPregunta(Pregunta preguntasIdPregunta) {
        this.preguntasIdPregunta = preguntasIdPregunta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRespuesta != null ? idRespuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Respuesta)) {
            return false;
        }
        Respuesta other = (Respuesta) object;
        if ((this.idRespuesta == null && other.idRespuesta != null) || (this.idRespuesta != null && !this.idRespuesta.equals(other.idRespuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Respuesta[ idRespuesta=" + idRespuesta + " ]";
    }
    
}
