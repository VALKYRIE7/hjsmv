/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "ubicaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ubicacion.findAll", query = "SELECT u FROM Ubicacion u")
    , @NamedQuery(name = "Ubicacion.findByIdUbicacion", query = "SELECT u FROM Ubicacion u WHERE u.idUbicacion = :idUbicacion")
    , @NamedQuery(name = "Ubicacion.findByUbicDireccion", query = "SELECT u FROM Ubicacion u WHERE u.ubicDireccion = :ubicDireccion")
    , @NamedQuery(name = "Ubicacion.findByUbicLatitud", query = "SELECT u FROM Ubicacion u WHERE u.ubicLatitud = :ubicLatitud")
    , @NamedQuery(name = "Ubicacion.findByUbicLongitud", query = "SELECT u FROM Ubicacion u WHERE u.ubicLongitud = :ubicLongitud")})
public class Ubicacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_ubicacion")
    private Integer idUbicacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ubic_direccion")
    private String ubicDireccion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ubic_latitud")
    private Float ubicLatitud;
    @Column(name = "ubic_longitud")
    private Float ubicLongitud;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ubicacionesIdUbicacion")
    private Collection<Inmueble> inmuebleCollection;
    @JoinColumn(name = "lugares_lugr_codigolugar", referencedColumnName = "lugr_codigolugar")
    @ManyToOne(optional = false)
    private Lugar lugaresLugrCodigolugar;

    public Ubicacion() {
    }

    public Ubicacion(Integer idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    public Ubicacion(Integer idUbicacion, String ubicDireccion) {
        this.idUbicacion = idUbicacion;
        this.ubicDireccion = ubicDireccion;
    }

    public Integer getIdUbicacion() {
        return idUbicacion;
    }

    public void setIdUbicacion(Integer idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    public String getUbicDireccion() {
        return ubicDireccion;
    }

    public void setUbicDireccion(String ubicDireccion) {
        this.ubicDireccion = ubicDireccion;
    }

    public Float getUbicLatitud() {
        return ubicLatitud;
    }

    public void setUbicLatitud(Float ubicLatitud) {
        this.ubicLatitud = ubicLatitud;
    }

    public Float getUbicLongitud() {
        return ubicLongitud;
    }

    public void setUbicLongitud(Float ubicLongitud) {
        this.ubicLongitud = ubicLongitud;
    }

    @XmlTransient
    public Collection<Inmueble> getInmuebleCollection() {
        return inmuebleCollection;
    }

    public void setInmuebleCollection(Collection<Inmueble> inmuebleCollection) {
        this.inmuebleCollection = inmuebleCollection;
    }

    public Lugar getLugaresLugrCodigolugar() {
        return lugaresLugrCodigolugar;
    }

    public void setLugaresLugrCodigolugar(Lugar lugaresLugrCodigolugar) {
        this.lugaresLugrCodigolugar = lugaresLugrCodigolugar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUbicacion != null ? idUbicacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ubicacion)) {
            return false;
        }
        Ubicacion other = (Ubicacion) object;
        if ((this.idUbicacion == null && other.idUbicacion != null) || (this.idUbicacion != null && !this.idUbicacion.equals(other.idUbicacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Ubicacion[ idUbicacion=" + idUbicacion + " ]";
    }
    
}
