/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "pagos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pago.findAll", query = "SELECT p FROM Pago p")
    , @NamedQuery(name = "Pago.findByIdPago", query = "SELECT p FROM Pago p WHERE p.idPago = :idPago")
    , @NamedQuery(name = "Pago.findByPagoFecha", query = "SELECT p FROM Pago p WHERE p.pagoFecha = :pagoFecha")
    , @NamedQuery(name = "Pago.findByPagoCantidad", query = "SELECT p FROM Pago p WHERE p.pagoCantidad = :pagoCantidad")})
public class Pago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_pago")
    private Integer idPago;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pago_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pagoFecha;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "pago_cantidad")
    private BigDecimal pagoCantidad;
    @JoinColumn(name = "reservas_id_reserva", referencedColumnName = "id_reserva")
    @ManyToOne(optional = false)
    private Reserva reservasIdReserva;

    public Pago() {
    }

    public Pago(Integer idPago) {
        this.idPago = idPago;
    }

    public Pago(Integer idPago, Date pagoFecha, BigDecimal pagoCantidad) {
        this.idPago = idPago;
        this.pagoFecha = pagoFecha;
        this.pagoCantidad = pagoCantidad;
    }

    public Integer getIdPago() {
        return idPago;
    }

    public void setIdPago(Integer idPago) {
        this.idPago = idPago;
    }

    public Date getPagoFecha() {
        return pagoFecha;
    }

    public void setPagoFecha(Date pagoFecha) {
        this.pagoFecha = pagoFecha;
    }

    public BigDecimal getPagoCantidad() {
        return pagoCantidad;
    }

    public void setPagoCantidad(BigDecimal pagoCantidad) {
        this.pagoCantidad = pagoCantidad;
    }

    public Reserva getReservasIdReserva() {
        return reservasIdReserva;
    }

    public void setReservasIdReserva(Reserva reservasIdReserva) {
        this.reservasIdReserva = reservasIdReserva;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPago != null ? idPago.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pago)) {
            return false;
        }
        Pago other = (Pago) object;
        if ((this.idPago == null && other.idPago != null) || (this.idPago != null && !this.idPago.equals(other.idPago))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Pago[ idPago=" + idPago + " ]";
    }
    
}
