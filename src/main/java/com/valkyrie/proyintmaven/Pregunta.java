/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "preguntas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pregunta.findAll", query = "SELECT p FROM Pregunta p")
    , @NamedQuery(name = "Pregunta.findByIdPregunta", query = "SELECT p FROM Pregunta p WHERE p.idPregunta = :idPregunta")
    , @NamedQuery(name = "Pregunta.findByPgtaTexto", query = "SELECT p FROM Pregunta p WHERE p.pgtaTexto = :pgtaTexto")
    , @NamedQuery(name = "Pregunta.findByPgtaFecha", query = "SELECT p FROM Pregunta p WHERE p.pgtaFecha = :pgtaFecha")})
public class Pregunta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_pregunta")
    private Integer idPregunta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "pgta_texto")
    private String pgtaTexto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pgta_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pgtaFecha;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "preguntasIdPregunta")
    private Collection<Respuesta> respuestaCollection;
    @JoinColumn(name = "inmuebles_id_inmueble", referencedColumnName = "id_inmueble")
    @ManyToOne(optional = false)
    private Inmueble inmueblesIdInmueble;

    public Pregunta() {
    }

    public Pregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public Pregunta(Integer idPregunta, String pgtaTexto, Date pgtaFecha) {
        this.idPregunta = idPregunta;
        this.pgtaTexto = pgtaTexto;
        this.pgtaFecha = pgtaFecha;
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getPgtaTexto() {
        return pgtaTexto;
    }

    public void setPgtaTexto(String pgtaTexto) {
        this.pgtaTexto = pgtaTexto;
    }

    public Date getPgtaFecha() {
        return pgtaFecha;
    }

    public void setPgtaFecha(Date pgtaFecha) {
        this.pgtaFecha = pgtaFecha;
    }

    @XmlTransient
    public Collection<Respuesta> getRespuestaCollection() {
        return respuestaCollection;
    }

    public void setRespuestaCollection(Collection<Respuesta> respuestaCollection) {
        this.respuestaCollection = respuestaCollection;
    }

    public Inmueble getInmueblesIdInmueble() {
        return inmueblesIdInmueble;
    }

    public void setInmueblesIdInmueble(Inmueble inmueblesIdInmueble) {
        this.inmueblesIdInmueble = inmueblesIdInmueble;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPregunta != null ? idPregunta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pregunta)) {
            return false;
        }
        Pregunta other = (Pregunta) object;
        if ((this.idPregunta == null && other.idPregunta != null) || (this.idPregunta != null && !this.idPregunta.equals(other.idPregunta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Pregunta[ idPregunta=" + idPregunta + " ]";
    }
    
}
