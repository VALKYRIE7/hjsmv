/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "empresas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empresa.findAll", query = "SELECT e FROM Empresa e")
    , @NamedQuery(name = "Empresa.findByIdEmpresa", query = "SELECT e FROM Empresa e WHERE e.idEmpresa = :idEmpresa")
    , @NamedQuery(name = "Empresa.findByEmprNombre", query = "SELECT e FROM Empresa e WHERE e.emprNombre = :emprNombre")
    , @NamedQuery(name = "Empresa.findByEmprTelefono", query = "SELECT e FROM Empresa e WHERE e.emprTelefono = :emprTelefono")
    , @NamedQuery(name = "Empresa.findByEmprFax", query = "SELECT e FROM Empresa e WHERE e.emprFax = :emprFax")
    , @NamedQuery(name = "Empresa.findByEmprSkype", query = "SELECT e FROM Empresa e WHERE e.emprSkype = :emprSkype")
    , @NamedQuery(name = "Empresa.findByEmprWhatsapp", query = "SELECT e FROM Empresa e WHERE e.emprWhatsapp = :emprWhatsapp")
    , @NamedQuery(name = "Empresa.findByEmprCorreo", query = "SELECT e FROM Empresa e WHERE e.emprCorreo = :emprCorreo")})
public class Empresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "empr_nombre")
    private String emprNombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "empr_telefono")
    private int emprTelefono;
    @Column(name = "empr_fax")
    private Integer emprFax;
    @Size(max = 20)
    @Column(name = "empr_skype")
    private String emprSkype;
    @Column(name = "empr_whatsapp")
    private Integer emprWhatsapp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "empr_correo")
    private String emprCorreo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresasIdEmpresa")
    private Collection<Inmueble> inmuebleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empresasIdEmpresa")
    private Collection<Identificacion> identificacionCollection;

    public Empresa() {
    }

    public Empresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Empresa(Integer idEmpresa, String emprNombre, int emprTelefono, String emprCorreo) {
        this.idEmpresa = idEmpresa;
        this.emprNombre = emprNombre;
        this.emprTelefono = emprTelefono;
        this.emprCorreo = emprCorreo;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getEmprNombre() {
        return emprNombre;
    }

    public void setEmprNombre(String emprNombre) {
        this.emprNombre = emprNombre;
    }

    public int getEmprTelefono() {
        return emprTelefono;
    }

    public void setEmprTelefono(int emprTelefono) {
        this.emprTelefono = emprTelefono;
    }

    public Integer getEmprFax() {
        return emprFax;
    }

    public void setEmprFax(Integer emprFax) {
        this.emprFax = emprFax;
    }

    public String getEmprSkype() {
        return emprSkype;
    }

    public void setEmprSkype(String emprSkype) {
        this.emprSkype = emprSkype;
    }

    public Integer getEmprWhatsapp() {
        return emprWhatsapp;
    }

    public void setEmprWhatsapp(Integer emprWhatsapp) {
        this.emprWhatsapp = emprWhatsapp;
    }

    public String getEmprCorreo() {
        return emprCorreo;
    }

    public void setEmprCorreo(String emprCorreo) {
        this.emprCorreo = emprCorreo;
    }

    @XmlTransient
    public Collection<Inmueble> getInmuebleCollection() {
        return inmuebleCollection;
    }

    public void setInmuebleCollection(Collection<Inmueble> inmuebleCollection) {
        this.inmuebleCollection = inmuebleCollection;
    }

    @XmlTransient
    public Collection<Identificacion> getIdentificacionCollection() {
        return identificacionCollection;
    }

    public void setIdentificacionCollection(Collection<Identificacion> identificacionCollection) {
        this.identificacionCollection = identificacionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmpresa != null ? idEmpresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empresa)) {
            return false;
        }
        Empresa other = (Empresa) object;
        if ((this.idEmpresa == null && other.idEmpresa != null) || (this.idEmpresa != null && !this.idEmpresa.equals(other.idEmpresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Empresa[ idEmpresa=" + idEmpresa + " ]";
    }
    
}
