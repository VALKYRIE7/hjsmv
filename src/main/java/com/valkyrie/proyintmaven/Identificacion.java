/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "identificaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Identificacion.findAll", query = "SELECT i FROM Identificacion i")
    , @NamedQuery(name = "Identificacion.findByIdIdentificacion", query = "SELECT i FROM Identificacion i WHERE i.idIdentificacion = :idIdentificacion")
    , @NamedQuery(name = "Identificacion.findByIdntNumero", query = "SELECT i FROM Identificacion i WHERE i.idntNumero = :idntNumero")
    , @NamedQuery(name = "Identificacion.findByIdntTipo", query = "SELECT i FROM Identificacion i WHERE i.idntTipo = :idntTipo")})
public class Identificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_identificacion")
    private Integer idIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idnt_numero")
    private int idntNumero;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "idnt_tipo")
    private String idntTipo;
    @JoinColumn(name = "empresas_id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa empresasIdEmpresa;

    public Identificacion() {
    }

    public Identificacion(Integer idIdentificacion) {
        this.idIdentificacion = idIdentificacion;
    }

    public Identificacion(Integer idIdentificacion, int idntNumero, String idntTipo) {
        this.idIdentificacion = idIdentificacion;
        this.idntNumero = idntNumero;
        this.idntTipo = idntTipo;
    }

    public Integer getIdIdentificacion() {
        return idIdentificacion;
    }

    public void setIdIdentificacion(Integer idIdentificacion) {
        this.idIdentificacion = idIdentificacion;
    }

    public int getIdntNumero() {
        return idntNumero;
    }

    public void setIdntNumero(int idntNumero) {
        this.idntNumero = idntNumero;
    }

    public String getIdntTipo() {
        return idntTipo;
    }

    public void setIdntTipo(String idntTipo) {
        this.idntTipo = idntTipo;
    }

    public Empresa getEmpresasIdEmpresa() {
        return empresasIdEmpresa;
    }

    public void setEmpresasIdEmpresa(Empresa empresasIdEmpresa) {
        this.empresasIdEmpresa = empresasIdEmpresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idIdentificacion != null ? idIdentificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Identificacion)) {
            return false;
        }
        Identificacion other = (Identificacion) object;
        if ((this.idIdentificacion == null && other.idIdentificacion != null) || (this.idIdentificacion != null && !this.idIdentificacion.equals(other.idIdentificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Identificacion[ idIdentificacion=" + idIdentificacion + " ]";
    }
    
}
