/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "inmuebles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inmueble.findAll", query = "SELECT i FROM Inmueble i")
    , @NamedQuery(name = "Inmueble.findByIdInmueble", query = "SELECT i FROM Inmueble i WHERE i.idInmueble = :idInmueble")
    , @NamedQuery(name = "Inmueble.findByInmbNombre", query = "SELECT i FROM Inmueble i WHERE i.inmbNombre = :inmbNombre")
    , @NamedQuery(name = "Inmueble.findByInmbAreainmb", query = "SELECT i FROM Inmueble i WHERE i.inmbAreainmb = :inmbAreainmb")
    , @NamedQuery(name = "Inmueble.findByInmbBano", query = "SELECT i FROM Inmueble i WHERE i.inmbBano = :inmbBano")
    , @NamedQuery(name = "Inmueble.findByInmbDescripcion", query = "SELECT i FROM Inmueble i WHERE i.inmbDescripcion = :inmbDescripcion")
    , @NamedQuery(name = "Inmueble.findByInmbPrecio", query = "SELECT i FROM Inmueble i WHERE i.inmbPrecio = :inmbPrecio")})
public class Inmueble implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_inmueble")
    private Integer idInmueble;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "inmb_nombre")
    private String inmbNombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "inmb_areainmb")
    private int inmbAreainmb;
    @Basic(optional = false)
    @NotNull
    @Column(name = "inmb_bano")
    private int inmbBano;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "inmb_descripcion")
    private String inmbDescripcion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "inmb_precio")
    private BigDecimal inmbPrecio;
    @ManyToMany(mappedBy = "inmuebleCollection")
    private Collection<Foto> fotoCollection;
    @ManyToMany(mappedBy = "inmuebleCollection")
    private Collection<Caracteristica> caracteristicaCollection;
    @JoinTable(name = "reservasinmuebles", joinColumns = {
        @JoinColumn(name = "inmuebles_id_inmueble", referencedColumnName = "id_inmueble")}, inverseJoinColumns = {
        @JoinColumn(name = "reservas_id_reserva", referencedColumnName = "id_reserva")})
    @ManyToMany
    private Collection<Reserva> reservaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "inmueblesIdInmueble")
    private Collection<Inventario> inventarioCollection;
    @JoinColumn(name = "empresas_id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa empresasIdEmpresa;
    @JoinColumn(name = "ubicaciones_id_ubicacion", referencedColumnName = "id_ubicacion")
    @ManyToOne(optional = false)
    private Ubicacion ubicacionesIdUbicacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "inmueblesIdInmueble")
    private Collection<Pregunta> preguntaCollection;

    public Inmueble() {
    }

    public Inmueble(Integer idInmueble) {
        this.idInmueble = idInmueble;
    }

    public Inmueble(Integer idInmueble, String inmbNombre, int inmbAreainmb, int inmbBano, String inmbDescripcion, BigDecimal inmbPrecio) {
        this.idInmueble = idInmueble;
        this.inmbNombre = inmbNombre;
        this.inmbAreainmb = inmbAreainmb;
        this.inmbBano = inmbBano;
        this.inmbDescripcion = inmbDescripcion;
        this.inmbPrecio = inmbPrecio;
    }

    public Integer getIdInmueble() {
        return idInmueble;
    }

    public void setIdInmueble(Integer idInmueble) {
        this.idInmueble = idInmueble;
    }

    public String getInmbNombre() {
        return inmbNombre;
    }

    public void setInmbNombre(String inmbNombre) {
        this.inmbNombre = inmbNombre;
    }

    public int getInmbAreainmb() {
        return inmbAreainmb;
    }

    public void setInmbAreainmb(int inmbAreainmb) {
        this.inmbAreainmb = inmbAreainmb;
    }

    public int getInmbBano() {
        return inmbBano;
    }

    public void setInmbBano(int inmbBano) {
        this.inmbBano = inmbBano;
    }

    public String getInmbDescripcion() {
        return inmbDescripcion;
    }

    public void setInmbDescripcion(String inmbDescripcion) {
        this.inmbDescripcion = inmbDescripcion;
    }

    public BigDecimal getInmbPrecio() {
        return inmbPrecio;
    }

    public void setInmbPrecio(BigDecimal inmbPrecio) {
        this.inmbPrecio = inmbPrecio;
    }

    @XmlTransient
    public Collection<Foto> getFotoCollection() {
        return fotoCollection;
    }

    public void setFotoCollection(Collection<Foto> fotoCollection) {
        this.fotoCollection = fotoCollection;
    }

    @XmlTransient
    public Collection<Caracteristica> getCaracteristicaCollection() {
        return caracteristicaCollection;
    }

    public void setCaracteristicaCollection(Collection<Caracteristica> caracteristicaCollection) {
        this.caracteristicaCollection = caracteristicaCollection;
    }

    @XmlTransient
    public Collection<Reserva> getReservaCollection() {
        return reservaCollection;
    }

    public void setReservaCollection(Collection<Reserva> reservaCollection) {
        this.reservaCollection = reservaCollection;
    }

    @XmlTransient
    public Collection<Inventario> getInventarioCollection() {
        return inventarioCollection;
    }

    public void setInventarioCollection(Collection<Inventario> inventarioCollection) {
        this.inventarioCollection = inventarioCollection;
    }

    public Empresa getEmpresasIdEmpresa() {
        return empresasIdEmpresa;
    }

    public void setEmpresasIdEmpresa(Empresa empresasIdEmpresa) {
        this.empresasIdEmpresa = empresasIdEmpresa;
    }

    public Ubicacion getUbicacionesIdUbicacion() {
        return ubicacionesIdUbicacion;
    }

    public void setUbicacionesIdUbicacion(Ubicacion ubicacionesIdUbicacion) {
        this.ubicacionesIdUbicacion = ubicacionesIdUbicacion;
    }

    @XmlTransient
    public Collection<Pregunta> getPreguntaCollection() {
        return preguntaCollection;
    }

    public void setPreguntaCollection(Collection<Pregunta> preguntaCollection) {
        this.preguntaCollection = preguntaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInmueble != null ? idInmueble.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inmueble)) {
            return false;
        }
        Inmueble other = (Inmueble) object;
        if ((this.idInmueble == null && other.idInmueble != null) || (this.idInmueble != null && !this.idInmueble.equals(other.idInmueble))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Inmueble[ idInmueble=" + idInmueble + " ]";
    }
    
}
