/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "caracteristicas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Caracteristica.findAll", query = "SELECT c FROM Caracteristica c")
    , @NamedQuery(name = "Caracteristica.findByIdCaracteristica", query = "SELECT c FROM Caracteristica c WHERE c.idCaracteristica = :idCaracteristica")
    , @NamedQuery(name = "Caracteristica.findByCrctNombre", query = "SELECT c FROM Caracteristica c WHERE c.crctNombre = :crctNombre")
    , @NamedQuery(name = "Caracteristica.findByCrctDisponible", query = "SELECT c FROM Caracteristica c WHERE c.crctDisponible = :crctDisponible")})
public class Caracteristica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_caracteristica")
    private Integer idCaracteristica;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "crct_nombre")
    private String crctNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "crct_disponible")
    private String crctDisponible;
    @JoinTable(name = "inmueblescaracteristicas", joinColumns = {
        @JoinColumn(name = "caracteristicas_id_caracteristica", referencedColumnName = "id_caracteristica")}, inverseJoinColumns = {
        @JoinColumn(name = "inmuebles_id_inmueble", referencedColumnName = "id_inmueble")})
    @ManyToMany
    private Collection<Inmueble> inmuebleCollection;

    public Caracteristica() {
    }

    public Caracteristica(Integer idCaracteristica) {
        this.idCaracteristica = idCaracteristica;
    }

    public Caracteristica(Integer idCaracteristica, String crctNombre, String crctDisponible) {
        this.idCaracteristica = idCaracteristica;
        this.crctNombre = crctNombre;
        this.crctDisponible = crctDisponible;
    }

    public Integer getIdCaracteristica() {
        return idCaracteristica;
    }

    public void setIdCaracteristica(Integer idCaracteristica) {
        this.idCaracteristica = idCaracteristica;
    }

    public String getCrctNombre() {
        return crctNombre;
    }

    public void setCrctNombre(String crctNombre) {
        this.crctNombre = crctNombre;
    }

    public String getCrctDisponible() {
        return crctDisponible;
    }

    public void setCrctDisponible(String crctDisponible) {
        this.crctDisponible = crctDisponible;
    }

    @XmlTransient
    public Collection<Inmueble> getInmuebleCollection() {
        return inmuebleCollection;
    }

    public void setInmuebleCollection(Collection<Inmueble> inmuebleCollection) {
        this.inmuebleCollection = inmuebleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCaracteristica != null ? idCaracteristica.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Caracteristica)) {
            return false;
        }
        Caracteristica other = (Caracteristica) object;
        if ((this.idCaracteristica == null && other.idCaracteristica != null) || (this.idCaracteristica != null && !this.idCaracteristica.equals(other.idCaracteristica))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Caracteristica[ idCaracteristica=" + idCaracteristica + " ]";
    }
    
}
