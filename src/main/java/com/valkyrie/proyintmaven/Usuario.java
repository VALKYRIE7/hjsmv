/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.proyintmaven;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Fernando Andrade
 */
@Entity
@Table(name = "usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByIdUsuario", query = "SELECT u FROM Usuario u WHERE u.idUsuario = :idUsuario")
    , @NamedQuery(name = "Usuario.findByUsrNombre", query = "SELECT u FROM Usuario u WHERE u.usrNombre = :usrNombre")
    , @NamedQuery(name = "Usuario.findByUsrApellido", query = "SELECT u FROM Usuario u WHERE u.usrApellido = :usrApellido")
    , @NamedQuery(name = "Usuario.findByUsrCorreo", query = "SELECT u FROM Usuario u WHERE u.usrCorreo = :usrCorreo")
    , @NamedQuery(name = "Usuario.findByUsrPassword", query = "SELECT u FROM Usuario u WHERE u.usrPassword = :usrPassword")
    , @NamedQuery(name = "Usuario.findByUsrIdentificacion", query = "SELECT u FROM Usuario u WHERE u.usrIdentificacion = :usrIdentificacion")
    , @NamedQuery(name = "Usuario.findByUsrTipoidentificacion", query = "SELECT u FROM Usuario u WHERE u.usrTipoidentificacion = :usrTipoidentificacion")
    , @NamedQuery(name = "Usuario.findByUsrTipousuario", query = "SELECT u FROM Usuario u WHERE u.usrTipousuario = :usrTipousuario")
    , @NamedQuery(name = "Usuario.findByUsrFechanacimiento", query = "SELECT u FROM Usuario u WHERE u.usrFechanacimiento = :usrFechanacimiento")})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "usr_nombre")
    private String usrNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "usr_apellido")
    private String usrApellido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "usr_correo")
    private String usrCorreo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "usr_password")
    private String usrPassword;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usr_identificacion")
    private int usrIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "usr_tipoidentificacion")
    private String usrTipoidentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "usr_tipousuario")
    private String usrTipousuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usr_fechanacimiento")
    @Temporal(TemporalType.DATE)
    private Date usrFechanacimiento;
    @ManyToMany(mappedBy = "usuarioCollection")
    private Collection<Reserva> reservaCollection;

    public Usuario() {
    }

    public Usuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Usuario(Integer idUsuario, String usrNombre, String usrApellido, String usrCorreo, String usrPassword, int usrIdentificacion, String usrTipoidentificacion, String usrTipousuario, Date usrFechanacimiento) {
        this.idUsuario = idUsuario;
        this.usrNombre = usrNombre;
        this.usrApellido = usrApellido;
        this.usrCorreo = usrCorreo;
        this.usrPassword = usrPassword;
        this.usrIdentificacion = usrIdentificacion;
        this.usrTipoidentificacion = usrTipoidentificacion;
        this.usrTipousuario = usrTipousuario;
        this.usrFechanacimiento = usrFechanacimiento;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsrNombre() {
        return usrNombre;
    }

    public void setUsrNombre(String usrNombre) {
        this.usrNombre = usrNombre;
    }

    public String getUsrApellido() {
        return usrApellido;
    }

    public void setUsrApellido(String usrApellido) {
        this.usrApellido = usrApellido;
    }

    public String getUsrCorreo() {
        return usrCorreo;
    }

    public void setUsrCorreo(String usrCorreo) {
        this.usrCorreo = usrCorreo;
    }

    public String getUsrPassword() {
        return usrPassword;
    }

    public void setUsrPassword(String usrPassword) {
        this.usrPassword = usrPassword;
    }

    public int getUsrIdentificacion() {
        return usrIdentificacion;
    }

    public void setUsrIdentificacion(int usrIdentificacion) {
        this.usrIdentificacion = usrIdentificacion;
    }

    public String getUsrTipoidentificacion() {
        return usrTipoidentificacion;
    }

    public void setUsrTipoidentificacion(String usrTipoidentificacion) {
        this.usrTipoidentificacion = usrTipoidentificacion;
    }

    public String getUsrTipousuario() {
        return usrTipousuario;
    }

    public void setUsrTipousuario(String usrTipousuario) {
        this.usrTipousuario = usrTipousuario;
    }

    public Date getUsrFechanacimiento() {
        return usrFechanacimiento;
    }

    public void setUsrFechanacimiento(Date usrFechanacimiento) {
        this.usrFechanacimiento = usrFechanacimiento;
    }

    @XmlTransient
    public Collection<Reserva> getReservaCollection() {
        return reservaCollection;
    }

    public void setReservaCollection(Collection<Reserva> reservaCollection) {
        this.reservaCollection = reservaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.valkyrie.proyintmaven.Usuario[ idUsuario=" + idUsuario + " ]";
    }
    
}
