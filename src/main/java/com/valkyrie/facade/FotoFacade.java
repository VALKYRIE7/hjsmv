/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Foto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Foto_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Inmueble;
import java.util.Collection;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class FotoFacade extends AbstractFacade<Foto> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FotoFacade() {
        super(Foto.class);
    }

    public boolean isInmuebleCollectionEmpty(Foto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Foto> foto = cq.from(Foto.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(foto, entity), cb.isNotEmpty(foto.get(Foto_.inmuebleCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Inmueble> findInmuebleCollection(Foto entity) {
        Foto mergedEntity = this.getMergedEntity(entity);
        Collection<Inmueble> inmuebleCollection = mergedEntity.getInmuebleCollection();
        inmuebleCollection.size();
        return inmuebleCollection;
    }

    @Override
    public Foto findWithParents(Foto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Foto> cq = cb.createQuery(Foto.class);
        Root<Foto> foto = cq.from(Foto.class);
        foto.fetch(Foto_.inmuebleCollection, JoinType.LEFT);
        cq.select(foto).where(cb.equal(foto, entity));
        try {
            return em.createQuery(cq).getSingleResult();
        } catch (Exception e) {
            return entity;
        }
    }
    
}
