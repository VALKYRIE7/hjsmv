/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Reserva;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Reserva_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Inmueble;
import com.valkyrie.proyintmaven.Usuario;
import com.valkyrie.proyintmaven.Pago;
import java.util.Collection;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class ReservaFacade extends AbstractFacade<Reserva> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReservaFacade() {
        super(Reserva.class);
    }

    public boolean isInmuebleCollectionEmpty(Reserva entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Reserva> reserva = cq.from(Reserva.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(reserva, entity), cb.isNotEmpty(reserva.get(Reserva_.inmuebleCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Inmueble> findInmuebleCollection(Reserva entity) {
        Reserva mergedEntity = this.getMergedEntity(entity);
        Collection<Inmueble> inmuebleCollection = mergedEntity.getInmuebleCollection();
        inmuebleCollection.size();
        return inmuebleCollection;
    }

    public boolean isUsuarioCollectionEmpty(Reserva entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Reserva> reserva = cq.from(Reserva.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(reserva, entity), cb.isNotEmpty(reserva.get(Reserva_.usuarioCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Usuario> findUsuarioCollection(Reserva entity) {
        Reserva mergedEntity = this.getMergedEntity(entity);
        Collection<Usuario> usuarioCollection = mergedEntity.getUsuarioCollection();
        usuarioCollection.size();
        return usuarioCollection;
    }

    public boolean isPagoCollectionEmpty(Reserva entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Reserva> reserva = cq.from(Reserva.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(reserva, entity), cb.isNotEmpty(reserva.get(Reserva_.pagoCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Pago> findPagoCollection(Reserva entity) {
        Reserva mergedEntity = this.getMergedEntity(entity);
        Collection<Pago> pagoCollection = mergedEntity.getPagoCollection();
        pagoCollection.size();
        return pagoCollection;
    }

    @Override
    public Reserva findWithParents(Reserva entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Reserva> cq = cb.createQuery(Reserva.class);
        Root<Reserva> reserva = cq.from(Reserva.class);
        reserva.fetch(Reserva_.usuarioCollection, JoinType.LEFT);
        cq.select(reserva).where(cb.equal(reserva, entity));
        try {
            return em.createQuery(cq).getSingleResult();
        } catch (Exception e) {
            return entity;
        }
    }
    
}
