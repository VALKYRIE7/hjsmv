/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Inventario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Inventario_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Inmueble;
import com.valkyrie.proyintmaven.Invelementos;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class InventarioFacade extends AbstractFacade<Inventario> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InventarioFacade() {
        super(Inventario.class);
    }

    public boolean isInmueblesIdInmuebleEmpty(Inventario entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Inventario> inventario = cq.from(Inventario.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(inventario, entity), cb.isNotNull(inventario.get(Inventario_.inmueblesIdInmueble)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Inmueble findInmueblesIdInmueble(Inventario entity) {
        return this.getMergedEntity(entity).getInmueblesIdInmueble();
    }

    public boolean isInvelementosIdElementoEmpty(Inventario entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Inventario> inventario = cq.from(Inventario.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(inventario, entity), cb.isNotNull(inventario.get(Inventario_.invelementosIdElemento)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Invelementos findInvelementosIdElemento(Inventario entity) {
        return this.getMergedEntity(entity).getInvelementosIdElemento();
    }
    
}
