/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Respuesta;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Respuesta_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Pregunta;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class RespuestaFacade extends AbstractFacade<Respuesta> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RespuestaFacade() {
        super(Respuesta.class);
    }

    public boolean isPreguntasIdPreguntaEmpty(Respuesta entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Respuesta> respuesta = cq.from(Respuesta.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(respuesta, entity), cb.isNotNull(respuesta.get(Respuesta_.preguntasIdPregunta)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Pregunta findPreguntasIdPregunta(Respuesta entity) {
        return this.getMergedEntity(entity).getPreguntasIdPregunta();
    }
    
}
