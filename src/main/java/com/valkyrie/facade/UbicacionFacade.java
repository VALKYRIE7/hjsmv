/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Ubicacion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Ubicacion_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Inmueble;
import com.valkyrie.proyintmaven.Lugar;
import java.util.Collection;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class UbicacionFacade extends AbstractFacade<Ubicacion> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UbicacionFacade() {
        super(Ubicacion.class);
    }

    public boolean isInmuebleCollectionEmpty(Ubicacion entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Ubicacion> ubicacion = cq.from(Ubicacion.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(ubicacion, entity), cb.isNotEmpty(ubicacion.get(Ubicacion_.inmuebleCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Inmueble> findInmuebleCollection(Ubicacion entity) {
        Ubicacion mergedEntity = this.getMergedEntity(entity);
        Collection<Inmueble> inmuebleCollection = mergedEntity.getInmuebleCollection();
        inmuebleCollection.size();
        return inmuebleCollection;
    }

    public boolean isLugaresLugrCodigolugarEmpty(Ubicacion entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Ubicacion> ubicacion = cq.from(Ubicacion.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(ubicacion, entity), cb.isNotNull(ubicacion.get(Ubicacion_.lugaresLugrCodigolugar)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Lugar findLugaresLugrCodigolugar(Ubicacion entity) {
        return this.getMergedEntity(entity).getLugaresLugrCodigolugar();
    }
    
}
