/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Lugar;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Lugar_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Lugar;
import com.valkyrie.proyintmaven.Lugar;
import com.valkyrie.proyintmaven.Ubicacion;
import java.util.Collection;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class LugarFacade extends AbstractFacade<Lugar> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LugarFacade() {
        super(Lugar.class);
    }

    public boolean isLugarCollectionEmpty(Lugar entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Lugar> lugar = cq.from(Lugar.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(lugar, entity), cb.isNotEmpty(lugar.get(Lugar_.lugarCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Lugar> findLugarCollection(Lugar entity) {
        Lugar mergedEntity = this.getMergedEntity(entity);
        Collection<Lugar> lugarCollection = mergedEntity.getLugarCollection();
        lugarCollection.size();
        return lugarCollection;
    }

    public boolean isLugaresCodigoubicacionEmpty(Lugar entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Lugar> lugar = cq.from(Lugar.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(lugar, entity), cb.isNotNull(lugar.get(Lugar_.lugaresCodigoubicacion)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Lugar findLugaresCodigoubicacion(Lugar entity) {
        return this.getMergedEntity(entity).getLugaresCodigoubicacion();
    }

    public boolean isUbicacionCollectionEmpty(Lugar entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Lugar> lugar = cq.from(Lugar.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(lugar, entity), cb.isNotEmpty(lugar.get(Lugar_.ubicacionCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Ubicacion> findUbicacionCollection(Lugar entity) {
        Lugar mergedEntity = this.getMergedEntity(entity);
        Collection<Ubicacion> ubicacionCollection = mergedEntity.getUbicacionCollection();
        ubicacionCollection.size();
        return ubicacionCollection;
    }
    
}
