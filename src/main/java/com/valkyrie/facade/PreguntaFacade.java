/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Pregunta;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Pregunta_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Respuesta;
import com.valkyrie.proyintmaven.Inmueble;
import java.util.Collection;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class PreguntaFacade extends AbstractFacade<Pregunta> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PreguntaFacade() {
        super(Pregunta.class);
    }

    public boolean isRespuestaCollectionEmpty(Pregunta entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Pregunta> pregunta = cq.from(Pregunta.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(pregunta, entity), cb.isNotEmpty(pregunta.get(Pregunta_.respuestaCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Respuesta> findRespuestaCollection(Pregunta entity) {
        Pregunta mergedEntity = this.getMergedEntity(entity);
        Collection<Respuesta> respuestaCollection = mergedEntity.getRespuestaCollection();
        respuestaCollection.size();
        return respuestaCollection;
    }

    public boolean isInmueblesIdInmuebleEmpty(Pregunta entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Pregunta> pregunta = cq.from(Pregunta.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(pregunta, entity), cb.isNotNull(pregunta.get(Pregunta_.inmueblesIdInmueble)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Inmueble findInmueblesIdInmueble(Pregunta entity) {
        return this.getMergedEntity(entity).getInmueblesIdInmueble();
    }
    
}
