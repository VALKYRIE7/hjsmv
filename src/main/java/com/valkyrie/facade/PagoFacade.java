/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Pago;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Pago_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Reserva;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class PagoFacade extends AbstractFacade<Pago> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PagoFacade() {
        super(Pago.class);
    }

    public boolean isReservasIdReservaEmpty(Pago entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Pago> pago = cq.from(Pago.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(pago, entity), cb.isNotNull(pago.get(Pago_.reservasIdReserva)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Reserva findReservasIdReserva(Pago entity) {
        return this.getMergedEntity(entity).getReservasIdReserva();
    }
    
}
