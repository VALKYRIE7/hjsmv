/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Invelementos;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Invelementos_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Inventario;
import com.valkyrie.proyintmaven.Invelementos;
import com.valkyrie.proyintmaven.Invelementos;
import java.util.Collection;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class InvelementosFacade extends AbstractFacade<Invelementos> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InvelementosFacade() {
        super(Invelementos.class);
    }

    public boolean isInventarioCollectionEmpty(Invelementos entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Invelementos> invelementos = cq.from(Invelementos.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(invelementos, entity), cb.isNotEmpty(invelementos.get(Invelementos_.inventarioCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Inventario> findInventarioCollection(Invelementos entity) {
        Invelementos mergedEntity = this.getMergedEntity(entity);
        Collection<Inventario> inventarioCollection = mergedEntity.getInventarioCollection();
        inventarioCollection.size();
        return inventarioCollection;
    }

    public boolean isInvelementosCollectionEmpty(Invelementos entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Invelementos> invelementos = cq.from(Invelementos.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(invelementos, entity), cb.isNotEmpty(invelementos.get(Invelementos_.invelementosCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Invelementos> findInvelementosCollection(Invelementos entity) {
        Invelementos mergedEntity = this.getMergedEntity(entity);
        Collection<Invelementos> invelementosCollection = mergedEntity.getInvelementosCollection();
        invelementosCollection.size();
        return invelementosCollection;
    }

    public boolean isInvelementosCodsubtipoEmpty(Invelementos entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Invelementos> invelementos = cq.from(Invelementos.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(invelementos, entity), cb.isNotNull(invelementos.get(Invelementos_.invelementosCodsubtipo)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Invelementos findInvelementosCodsubtipo(Invelementos entity) {
        return this.getMergedEntity(entity).getInvelementosCodsubtipo();
    }
    
}
