/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Empresa;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Empresa_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Inmueble;
import com.valkyrie.proyintmaven.Identificacion;
import java.util.Collection;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class EmpresaFacade extends AbstractFacade<Empresa> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmpresaFacade() {
        super(Empresa.class);
    }

    public boolean isInmuebleCollectionEmpty(Empresa entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Empresa> empresa = cq.from(Empresa.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(empresa, entity), cb.isNotEmpty(empresa.get(Empresa_.inmuebleCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Inmueble> findInmuebleCollection(Empresa entity) {
        Empresa mergedEntity = this.getMergedEntity(entity);
        Collection<Inmueble> inmuebleCollection = mergedEntity.getInmuebleCollection();
        inmuebleCollection.size();
        return inmuebleCollection;
    }

    public boolean isIdentificacionCollectionEmpty(Empresa entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Empresa> empresa = cq.from(Empresa.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(empresa, entity), cb.isNotEmpty(empresa.get(Empresa_.identificacionCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Identificacion> findIdentificacionCollection(Empresa entity) {
        Empresa mergedEntity = this.getMergedEntity(entity);
        Collection<Identificacion> identificacionCollection = mergedEntity.getIdentificacionCollection();
        identificacionCollection.size();
        return identificacionCollection;
    }
    
}
