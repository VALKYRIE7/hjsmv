/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Identificacion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Identificacion_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Empresa;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class IdentificacionFacade extends AbstractFacade<Identificacion> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public IdentificacionFacade() {
        super(Identificacion.class);
    }

    public boolean isEmpresasIdEmpresaEmpty(Identificacion entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Identificacion> identificacion = cq.from(Identificacion.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(identificacion, entity), cb.isNotNull(identificacion.get(Identificacion_.empresasIdEmpresa)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Empresa findEmpresasIdEmpresa(Identificacion entity) {
        return this.getMergedEntity(entity).getEmpresasIdEmpresa();
    }
    
}
