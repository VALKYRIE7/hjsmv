/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Caracteristica;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Caracteristica_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Inmueble;
import java.util.Collection;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class CaracteristicaFacade extends AbstractFacade<Caracteristica> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CaracteristicaFacade() {
        super(Caracteristica.class);
    }

    public boolean isInmuebleCollectionEmpty(Caracteristica entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Caracteristica> caracteristica = cq.from(Caracteristica.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(caracteristica, entity), cb.isNotEmpty(caracteristica.get(Caracteristica_.inmuebleCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Inmueble> findInmuebleCollection(Caracteristica entity) {
        Caracteristica mergedEntity = this.getMergedEntity(entity);
        Collection<Inmueble> inmuebleCollection = mergedEntity.getInmuebleCollection();
        inmuebleCollection.size();
        return inmuebleCollection;
    }

    @Override
    public Caracteristica findWithParents(Caracteristica entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Caracteristica> cq = cb.createQuery(Caracteristica.class);
        Root<Caracteristica> caracteristica = cq.from(Caracteristica.class);
        caracteristica.fetch(Caracteristica_.inmuebleCollection, JoinType.LEFT);
        cq.select(caracteristica).where(cb.equal(caracteristica, entity));
        try {
            return em.createQuery(cq).getSingleResult();
        } catch (Exception e) {
            return entity;
        }
    }
    
}
