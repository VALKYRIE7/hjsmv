/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valkyrie.facade;

import com.valkyrie.proyintmaven.Inmueble;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.valkyrie.proyintmaven.Inmueble_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import com.valkyrie.proyintmaven.Foto;
import com.valkyrie.proyintmaven.Caracteristica;
import com.valkyrie.proyintmaven.Reserva;
import com.valkyrie.proyintmaven.Inventario;
import com.valkyrie.proyintmaven.Empresa;
import com.valkyrie.proyintmaven.Ubicacion;
import com.valkyrie.proyintmaven.Pregunta;
import java.util.Collection;

/**
 *
 * @author Fernando Andrade
 */
@Stateless
public class InmuebleFacade extends AbstractFacade<Inmueble> {

    @PersistenceContext(unitName = "com.valkyrie_proyIntMaven_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InmuebleFacade() {
        super(Inmueble.class);
    }

    public boolean isFotoCollectionEmpty(Inmueble entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Inmueble> inmueble = cq.from(Inmueble.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(inmueble, entity), cb.isNotEmpty(inmueble.get(Inmueble_.fotoCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Foto> findFotoCollection(Inmueble entity) {
        Inmueble mergedEntity = this.getMergedEntity(entity);
        Collection<Foto> fotoCollection = mergedEntity.getFotoCollection();
        fotoCollection.size();
        return fotoCollection;
    }

    public boolean isCaracteristicaCollectionEmpty(Inmueble entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Inmueble> inmueble = cq.from(Inmueble.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(inmueble, entity), cb.isNotEmpty(inmueble.get(Inmueble_.caracteristicaCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Caracteristica> findCaracteristicaCollection(Inmueble entity) {
        Inmueble mergedEntity = this.getMergedEntity(entity);
        Collection<Caracteristica> caracteristicaCollection = mergedEntity.getCaracteristicaCollection();
        caracteristicaCollection.size();
        return caracteristicaCollection;
    }

    public boolean isReservaCollectionEmpty(Inmueble entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Inmueble> inmueble = cq.from(Inmueble.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(inmueble, entity), cb.isNotEmpty(inmueble.get(Inmueble_.reservaCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Reserva> findReservaCollection(Inmueble entity) {
        Inmueble mergedEntity = this.getMergedEntity(entity);
        Collection<Reserva> reservaCollection = mergedEntity.getReservaCollection();
        reservaCollection.size();
        return reservaCollection;
    }

    public boolean isInventarioCollectionEmpty(Inmueble entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Inmueble> inmueble = cq.from(Inmueble.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(inmueble, entity), cb.isNotEmpty(inmueble.get(Inmueble_.inventarioCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Inventario> findInventarioCollection(Inmueble entity) {
        Inmueble mergedEntity = this.getMergedEntity(entity);
        Collection<Inventario> inventarioCollection = mergedEntity.getInventarioCollection();
        inventarioCollection.size();
        return inventarioCollection;
    }

    public boolean isEmpresasIdEmpresaEmpty(Inmueble entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Inmueble> inmueble = cq.from(Inmueble.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(inmueble, entity), cb.isNotNull(inmueble.get(Inmueble_.empresasIdEmpresa)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Empresa findEmpresasIdEmpresa(Inmueble entity) {
        return this.getMergedEntity(entity).getEmpresasIdEmpresa();
    }

    public boolean isUbicacionesIdUbicacionEmpty(Inmueble entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Inmueble> inmueble = cq.from(Inmueble.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(inmueble, entity), cb.isNotNull(inmueble.get(Inmueble_.ubicacionesIdUbicacion)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Ubicacion findUbicacionesIdUbicacion(Inmueble entity) {
        return this.getMergedEntity(entity).getUbicacionesIdUbicacion();
    }

    public boolean isPreguntaCollectionEmpty(Inmueble entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Inmueble> inmueble = cq.from(Inmueble.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(inmueble, entity), cb.isNotEmpty(inmueble.get(Inmueble_.preguntaCollection)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Collection<Pregunta> findPreguntaCollection(Inmueble entity) {
        Inmueble mergedEntity = this.getMergedEntity(entity);
        Collection<Pregunta> preguntaCollection = mergedEntity.getPreguntaCollection();
        preguntaCollection.size();
        return preguntaCollection;
    }

    @Override
    public Inmueble findWithParents(Inmueble entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Inmueble> cq = cb.createQuery(Inmueble.class);
        Root<Inmueble> inmueble = cq.from(Inmueble.class);
        inmueble.fetch(Inmueble_.reservaCollection, JoinType.LEFT);
        cq.select(inmueble).where(cb.equal(inmueble, entity));
        try {
            return em.createQuery(cq).getSingleResult();
        } catch (Exception e) {
            return entity;
        }
    }
    
}
